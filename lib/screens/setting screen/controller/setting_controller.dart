import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/keys.dart';
import 'package:blue_works/services/services.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class SettingController extends GetxController {
  static SettingController instance = Get.find();
  final GlobalKey<FormState> changePasswordKey = GlobalKey();
  TextEditingController oldPasswordCtrl = TextEditingController();
  TextEditingController newPasswordCtrl = TextEditingController();
  TextEditingController confirmPasswordCtrl = TextEditingController();
  RxBool oldPasswordObsecure = true.obs;
  RxBool newPasswordObsecure = true.obs;
  RxBool confirmPasswordObsecure = true.obs;
  RxBool isChangingPassword = false.obs;

  ///This is used to clear change password textform fields value
  clearTextFormFields() {
    oldPasswordCtrl.text = "";
    newPasswordCtrl.text = "";
    confirmPasswordCtrl.text = "";
  }

  ///This is used to change the obsecure text of password to default when page is disposed
  changeObsecureToDefault() {
    oldPasswordObsecure(true);
    newPasswordObsecure(true);
    confirmPasswordObsecure(true);
  }

  _changePassword(String oldPass, String newPass) async {
    try {
      isChangingPassword(true);
      await Services.changePassword(oldPassword: oldPass, newPassword: newPass);
    } finally {
      isChangingPassword(false);
    }
  }

  validateTextFormField(
      {required String oldPass,
      required String newPass,
      required String confirmPass}) {
    if (oldPass.isEmpty || newPass.isEmpty || confirmPass.isEmpty) {
      DialogueBoxes.flutterToastDialogueBox("All Fields required",
          color: AppColors.errorColor);
    } else if (newPass != confirmPass) {
      DialogueBoxes.flutterToastDialogueBox(
          "New and confirm password does not match",
          color: AppColors.errorColor);
    } else if (Keys.passwordRegx.hasMatch(newPass)) {
      _changePassword(oldPass, newPass);
    } else {
      DialogueBoxes.flutterToastDialogueBox(
          "Password must contain at least 1 uppercase letter, 3 digits, and be between 8 and 15 characters long",
          color: AppColors.errorColor,
          length: Toast.LENGTH_LONG);
    }
  }
}
