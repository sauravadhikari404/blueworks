import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/app%20bar/mero_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/color.dart';
import '../../../widgets/text form field/text_form_field.dart';
import '../../auth screens/widgets/button.dart';

class ChangePasswordPage extends StatelessWidget {
  const ChangePasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        globalCtrl.unFocusKeybaord(context);
      },
      child: Scaffold(
        appBar: MeroAppBar(
          title: TextConst.changePass,
          leading: IconButton(
              onPressed: () {
                Get.back();
                settingCtrl.clearTextFormFields();
                settingCtrl.changeObsecureToDefault();
              },
              icon: const Icon(Icons.arrow_back_ios_new)),
        ),
        body: GestureDetector(
          onTap: () {
            globalCtrl.unFocusKeybaord(context);
          },
          child: WillPopScope(
            onWillPop: () async {
              Get.back();
              settingCtrl.clearTextFormFields();
              settingCtrl.changeObsecureToDefault();
              return true;
            },
            child: SingleChildScrollView(
              child: Padding(
                padding: bodyOnlyPadding(context),
                child: Form(
                  key: settingCtrl.changePasswordKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _text(text: TextConst.oldPass, context: context),
                      SizedBox(
                        height: 0.010.h(context),
                      ),
                      Obx(() => TextFormFieldWidget(
                          title: TextConst.oldPass,
                          obsecure: settingCtrl.oldPasswordObsecure.value,
                          controller: settingCtrl.oldPasswordCtrl,
                          suffixIcon: IconButton(
                              onPressed: () {
                                settingCtrl.oldPasswordObsecure.toggle();
                              },
                              icon: settingCtrl.oldPasswordObsecure.value
                                  ? const Icon(Icons.remove_red_eye_outlined)
                                  : const Icon(Icons.visibility_off)))),
                      SizedBox(
                        height: 0.020.h(context),
                      ),
                      _text(text: TextConst.newPass, context: context),
                      SizedBox(
                        height: 0.010.h(context),
                      ),
                      Obx(
                        () => TextFormFieldWidget(
                            title: TextConst.newPass,
                            obsecure: settingCtrl.newPasswordObsecure.value,
                            controller: settingCtrl.newPasswordCtrl,
                            suffixIcon: IconButton(
                                onPressed: () {
                                  settingCtrl.newPasswordObsecure.toggle();
                                },
                                icon: settingCtrl.newPasswordObsecure.value
                                    ? const Icon(Icons.remove_red_eye_outlined)
                                    : const Icon(Icons.visibility_off))),
                      ),
                      SizedBox(
                        height: 0.020.h(context),
                      ),
                      _text(text: TextConst.confirmPass, context: context),
                      SizedBox(
                        height: 0.010.h(context),
                      ),
                      Obx(() => TextFormFieldWidget(
                          title: TextConst.confirmPass,
                          obsecure: settingCtrl.confirmPasswordObsecure.value,
                          controller: settingCtrl.confirmPasswordCtrl,
                          suffixIcon: IconButton(
                              onPressed: () {
                                settingCtrl.confirmPasswordObsecure.toggle();
                              },
                              icon: settingCtrl.confirmPasswordObsecure.value
                                  ? const Icon(Icons.remove_red_eye_outlined)
                                  : const Icon(Icons.visibility_off)))),
                      SizedBox(
                        height: 0.030.h(context),
                      ),
                      Obx(
                        () => settingCtrl.isChangingPassword.isTrue
                            ? Center(
                                child: CircularProgressIndicator(
                                  color: AppColors.appThemeColor,
                                ),
                              )
                            : SizedBox(
                                width: double.infinity,
                                height: 0.055.h(context),
                                child: ElevatedButtonWiddget(
                                  text: TextConst.changePass,
                                  fontFamily: "Roboto",
                                  textFontSize: 0.014.toResponsive(context),
                                  textColor: Colors.white,
                                  tappedFunction: () {
                                    if (settingCtrl
                                        .changePasswordKey.currentState!
                                        .validate()) {
                                      settingCtrl.validateTextFormField(
                                          oldPass:
                                              settingCtrl.oldPasswordCtrl.text,
                                          newPass:
                                              settingCtrl.newPasswordCtrl.text,
                                          confirmPass: settingCtrl
                                              .confirmPasswordCtrl.text);
                                    }
                                  },
                                  backgroundColor: AppColors.appThemeColor,
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

Widget _text({required String text, required BuildContext context}) {
  return Text(
    text,
    style: TextStyle(
        fontSize: 0.014.toResponsive(context),
        fontWeight: FontWeight.w400,
        fontFamily: "Roboto"),
  );
}
