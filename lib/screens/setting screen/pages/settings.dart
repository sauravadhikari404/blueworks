import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/screens/setting%20screen/controller/setting_controller.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/image_assets.dart';
import '../../../widgets/fade in image/fade_in_image_widget.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({super.key});

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  void initState() {
    Get.put(SettingController());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: 0.035.h(context),
          ),
          _image(context),
          Align(
              alignment: Alignment.center,
              child: Text(
                globalCtrl.concatName(
                    firstName:
                        homePageCtrl.userProfileData.value?.user?.firstName ??
                            "",
                    middleName:
                        homePageCtrl.userProfileData.value?.user?.middleName,
                    lastName:
                        homePageCtrl.userProfileData.value?.user?.lastName ??
                            ""),
                style: TextStyle(
                    fontSize: 0.016.toResponsive(context),
                    fontWeight: FontWeight.w400,
                    fontFamily: "Roboto"),
              )),
          SizedBox(
            height: 0.020.h(context),
          ),
          Padding(
            padding: bodyOnlyPadding(context),
            child: Text(
              TextConst.general,
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: 0.012.toResponsive(context),
                  fontFamily: "Roboto"),
            ),
          ),
          SizedBox(
            height: 0.010.h(context),
          ),
          _listTile(Icons.person, TextConst.editProf,
              () => Get.toNamed(MeroRoutesName.editProfilePage)),
          _divider(),
          _listTile(Icons.change_circle, TextConst.changeTheme, () {}),
          _divider(),
          _listTile(
              Icons.logout, TextConst.logout, () => globalCtrl.logoutUser()),
          _divider(),
          SizedBox(
            height: 0.020.h(context),
          ),
          Padding(
            padding: bodyOnlyPadding(context),
            child: Text(
              TextConst.security,
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: 0.012.toResponsive(context),
                  fontFamily: "Roboto"),
            ),
          ),
          SizedBox(
            height: 0.010.h(context),
          ),
          _listTile(Icons.password, TextConst.changePass,
              () => Get.toNamed(MeroRoutesName.changePasswordPage)),
          _divider(),
        ],
      ),
    ));
  }

  Widget _image(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: SizedBox(
        height: 0.2.h(context),
        width: 0.45.w(context),
        child: AspectRatio(
          aspectRatio: 1 / 1,
          child: ClipOval(
            child: FadedImageWidget(
                imageUrl: "${homePageCtrl.userProfileData.value?.profilePic}"
                    .simpleMedia(),
                imagePlaceHolder: AssetConst.profile,
                imageError: AssetConst.profile),
          ),
        ),
      ),
    );
  }

  Widget _listTile(IconData icon, String title, VoidCallback tappedFunction) {
    return ListTile(
      leading: Icon(
        icon,
        size: 0.020.toResponsive(context),
      ),
      title: Text(
        title,
        style: TextStyle(
            fontSize: 0.014.toResponsive(context), fontFamily: "Roboto"),
      ),
      style: ListTileStyle.list,
      onTap: tappedFunction,
    );
  }

  Widget _divider() {
    return Padding(
      padding: EdgeInsets.only(left: 0.020.w(context), right: 0.020.w(context)),
      child: Divider(
        color: AppColors.appThemeColor,
        thickness: 1,
      ),
    );
  }
}
