import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/enum.dart';
import 'package:blue_works/screens/profession%20listing%20screen/controller/professionals_controller.dart';
import 'package:blue_works/widgets/app%20bar/mero_app_bar.dart';
import 'package:blue_works/widgets/custom%20loading%20spinner/loading_spinner.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/padding.dart';
import '../../../route/route_constants.dart';
import '../../../models/professional_category_model.dart';
import '../../../widgets/dialogue boxes/dialogue_boxes.dart';
import '../../../widgets/professionals widgets/professional_listing_page_design_widget.dart';
import '../../../widgets/rich text/rich_text.dart';
import '../../../widgets/text form field/search_text_form.dart';

class ProfessionListingPage extends StatefulWidget {
  const ProfessionListingPage({super.key});

  @override
  State<ProfessionListingPage> createState() => _ProfessionListingPageState();
}

class _ProfessionListingPageState extends State<ProfessionListingPage> {
  ProfessionalCategoryModel profCatData = Get.arguments[0];
  final ScrollController _scrollController = ScrollController();
  int pk = Get.arguments[1];
  TextEditingController searchTxt = TextEditingController();
  double latitude = 0.0;
  double longitude = 0.0;
  int page = 1;

  void pagination() {
    try {
      if ((_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent)) {
        if (((profCtrl.professionalData.value.pagination?.totalPages ?? 0) >
            (page))) {
          setState(() {
            page = page + 1;
            profCtrl
                .fetchProfessionals(page: page, isLoadingMore: true, pk: pk)
                .then((value) => null)
                .whenComplete(() => profCtrl.isLoadMore.value = false);
          });
        }
      }
    } catch (e) {
      DialogueBoxes.flutterToastDialogueBox("Error occurred",
          color: AppColors.errorColor);
      rethrow;
    }
  }

  @override
  void initState() {
    Get.put(ProfessionalsController());
    _scrollController.addListener(pagination);
    profCtrl.fetchProfessionals(pk: pk, searchText: searchTxt.text, page: page);
    super.initState();
  }

  @override
  void dispose() {
    searchTxt.dispose();
    globalCtrl.timer?.cancel();
    page = 1;
    profCtrl.toggleNearBy.value = false;
    latitude = 0.0;
    longitude = 0.0;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        globalCtrl.unFocusKeybaord(context);
      },
      child: Scaffold(
        appBar: MeroAppBar(
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: const Icon(Icons.arrow_back_ios_new)),
          title: "${profCatData.name?.replaceAll("-", " ")}",
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            profCtrl.fetchProfessionals(
                pk: pk,
                page: page,
                searchText: searchTxt.text,
                latitude: latitude == 0.0 ? null : latitude,
                longitude: longitude == 0.0 ? null : longitude);
          },
          child: Column(
            children: [
              SizedBox(
                height: 0.02.h(context),
              ),
              Padding(
                padding: bodyLeftRightPadding(context),
                child: RichTextStyle.richTextWidget(
                    context: context,
                    text1: "Note : ",
                    text1FontSize: 0.012.toResponsive(context),
                    text2FontSize: 0.011.toResponsive(context),
                    color1: Colors.black,
                    color: Colors.black54,
                    fontFamily: "Roboto",
                    text2:
                        "Tap on the first icon inside search field to get near by professions"),
              ),
              SizedBox(
                height: 0.02.h(context),
              ),
              Padding(
                padding: bodyLeftRightPadding(context),
                child: SearchTextFormField(
                  title: "Search by name or address",
                  suffixIcon: const Icon(
                    Icons.search,
                    color: Colors.black38,
                  ),
                  prefixIcon: nearMeIcon(),
                  controller: searchTxt,
                  pk: pk,
                  whichPage: WHICHPAGE.professionalPage,
                ),
              ),
              Obx(
                () => profCtrl.isLoading.isTrue
                    ? LoadingSpinner.showLoadingSpinner(
                        context: context,
                        text:
                            "Fetching ${profCatData.name?.replaceAll("-", " ")} List")
                    : (profCtrl.professionalData.value.results ?? []).isEmpty
                        ? const Align(
                            alignment: Alignment.bottomCenter,
                            child: Text(
                              "No Data found!",
                            ),
                          )
                        : SingleChildScrollView(
                            controller: _scrollController,
                            physics: const ClampingScrollPhysics(
                                parent: AlwaysScrollableScrollPhysics()),
                            child: Column(
                              children: [
                                Padding(
                                  padding: bodyOnlyPadding(context),
                                  child: ListView.builder(
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: (profCtrl.professionalData
                                                  .value.results ??
                                              [])
                                          .length,
                                      itemBuilder: (ctx, i) {
                                        var data = profCtrl
                                            .professionalData.value.results?[i];
                                        return Padding(
                                            padding: EdgeInsets.symmetric(
                                                vertical: 0.010.h(context)),
                                            child: GestureDetector(
                                              onTap: () {
                                                Get.toNamed(
                                                    MeroRoutesName
                                                        .professionalsDetailPage,
                                                    arguments: [
                                                      globalCtrl.concatName(
                                                          firstName: data?.user
                                                                  ?.firstName ??
                                                              "",
                                                          middleName: data?.user
                                                              ?.middleName,
                                                          lastName: data?.user
                                                                  ?.lastName ??
                                                              ""),
                                                      data?.dob,
                                                      data?.profession,
                                                      "${data?.address?.country}, ${data?.address?.city}, ${data?.address?.street}",
                                                      data?.profilePic,
                                                      data?.user?.id
                                                    ]);
                                              },
                                              child:
                                                  ProfessionalListingDesignWidget(
                                                profilePic: data?.profilePic,
                                                name: globalCtrl.concatName(
                                                    firstName:
                                                        data?.user?.firstName ??
                                                            "",
                                                    lastName:
                                                        data?.user?.lastName ??
                                                            "",
                                                    middleName:
                                                        data?.user?.middleName),
                                                profession: data?.profession,
                                              ),
                                            ));
                                      }),
                                ),
                                profCtrl.isLoadMore.value
                                    ? Center(
                                        child: CircularProgressIndicator(
                                          color: AppColors.appThemeColor,
                                        ),
                                      )
                                    : const SizedBox()
                              ],
                            ),
                          ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget nearMeIcon() {
    return GestureDetector(
        onTap: () async {
          if (networkCtrl.connectionType.value == 1 ||
              networkCtrl.connectionType.value == 2) {
            profCtrl.toggleNearBy.toggle();
            if (profCtrl.toggleNearBy.isTrue) {
              profCtrl.isLoading(true);
              var data = await globalCtrl.determinePosition();
              latitude = data.latitude;
              longitude = data.longitude;
              profCtrl.fetchProfessionals(
                  pk: pk,
                  page: page,
                  searchText: searchTxt.text,
                  latitude: latitude,
                  longitude: longitude);
            }
          } else if (networkCtrl.connectionType.value == 0) {
            DialogueBoxes.flutterToastDialogueBox(
                "Please check you internet connection",
                color: AppColors.errorColor);
          }
        },
        child: Obx(
          () => Icon(
            Icons.near_me,
            color: profCtrl.toggleNearBy.value == true
                ? AppColors.appThemeColor
                : Colors.black54,
          ),
        ));
  }
}
