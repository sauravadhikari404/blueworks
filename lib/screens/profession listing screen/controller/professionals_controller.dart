import 'package:blue_works/services/services.dart';
import 'package:get/get.dart';

import '../../../constants/keys.dart';
import '../model/professionals_model.dart';

class ProfessionalsController extends GetxController {
  static ProfessionalsController instance = Get.find();
  RxBool isLoading = true.obs;
  RxBool isLoadMore = false.obs;
  Rx<ProfessionalsModel> professionalData = Rx(ProfessionalsModel());
  RxBool toggleNearBy = false.obs;

  Future<void> fetchProfessionals(
      {bool isLoadingMore = false,
      String searchText = "",
      double? latitude,
      double? longitude,
      required int pk,
      required int page}) async {
    try {
      logger.i("THis is on changed");
      isLoadingMore ? isLoadMore(true) : isLoading(true);
      var data = await Services.fetchProfessionals(
          searchText: searchText,
          pk: pk,
          latitude: latitude,
          longitude: longitude,
          page: page);
      isLoadingMore
          ? professionalData.value.results?.addAll(data.results ?? [])
          : professionalData.value = data;
    } finally {
      isLoading(false);
      isLoadMore(false);
    }
  }
}
