import 'dart:convert';

import '../../../models/pagination_model.dart';

ProfessionalsModel professionalsModelFromJson(String str) =>
    ProfessionalsModel.fromJson(json.decode(str));

String professionalsModelToJson(ProfessionalsModel data) =>
    json.encode(data.toJson());

class ProfessionalsModel {
  ProfessionalsModel({
    this.pagination,
    this.results,
  });

  Pagination? pagination;
  List<Result>? results;

  factory ProfessionalsModel.fromJson(Map<String, dynamic> json) =>
      ProfessionalsModel(
        pagination: json["pagination"] == null
            ? null
            : Pagination.fromJson(json["pagination"]),
        results: json["results"] == null
            ? []
            : List<Result>.from(
                json["results"]!.map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "pagination": pagination?.toJson(),
        "results": results == null
            ? []
            : List<dynamic>.from(results!.map((x) => x.toJson())),
      };
}

class Result {
  Result({
    this.id,
    this.bio,
    this.dob,
    this.user,
    this.address,
    this.profession,
    this.profilePic,
    this.followers,
    this.following,
    this.grids,
  });

  int? id;
  String? bio;
  DateTime? dob;
  User? user;
  Address? address;
  List<String>? profession;
  String? profilePic;
  int? followers;
  int? following;
  int? grids;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        bio: json["bio"],
        dob: json["dob"] == null ? null : DateTime.parse(json["dob"]),
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        address:
            json["address"] == null ? null : Address.fromJson(json["address"]),
        profession: json["profession"] == null
            ? []
            : List<String>.from(json["profession"]!.map((x) => x)),
        profilePic: json["profile_pic"],
        followers: json["followers"],
        following: json["following"],
        grids: json["grids"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bio": bio,
        "dob":
            "${dob!.year.toString().padLeft(4, '0')}-${dob!.month.toString().padLeft(2, '0')}-${dob!.day.toString().padLeft(2, '0')}",
        "user": user?.toJson(),
        "address": address?.toJson(),
        "profession": profession == null
            ? []
            : List<dynamic>.from(profession!.map((x) => x)),
        "profile_pic": profilePic,
        "followers": followers,
        "following": following,
        "grids": grids,
      };
}

class Address {
  Address({
    this.city,
    this.country,
    this.street,
  });

  String? city;
  String? country;
  String? street;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        city: json["city"],
        country: json["country"],
        street: json["street"],
      );

  Map<String, dynamic> toJson() => {
        "city": city,
        "country": country,
        "street": street,
      };
}

class User {
  User({
    this.id,
    this.firstName,
    this.middleName,
    this.lastName,
  });

  int? id;
  String? firstName;
  String? middleName;
  String? lastName;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstName: json["first_name"],
        middleName: json["middle_name"],
        lastName: json["last_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "middle_name": middleName,
        "last_name": lastName,
      };
}
