import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

import '../../../constants/controllers.dart';
import '../../../widgets/rich text/rich_text.dart';
import '../widgets/home_page_banner_widget.dart';
import '../widgets/home_page_category_widget.dart';
import '../widgets/slider_widget.dart';
import '../widgets/top_professionals_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: 0.055.h(context),
                  left: 0.040.w(context),
                  right: 0.040.w(context)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Obx(() => homePageCtrl.isLoading.value
                      ? Shimmer.fromColors(
                          baseColor: AppColors.appThemeColor,
                          highlightColor: Colors.yellow,
                          child: Text(
                            'Hi, Name',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 0.014.toResponsive(context),
                                fontWeight: FontWeight.bold,
                                fontFamily: "Roboto"),
                          ),
                        )
                      : RichTextStyle.richTextWidget(
                          context: context,
                          text1: "${TextConst.hi},",
                          fontFamily: "Roboto",
                          text2: globalCtrl.concatName(
                              firstName: homePageCtrl
                                      .userProfileData.value?.user?.firstName ??
                                  "",
                              middleName: homePageCtrl
                                  .userProfileData.value?.user?.middleName,
                              lastName: homePageCtrl
                                      .userProfileData.value?.user?.lastName ??
                                  ""),
                          color1: AppColors.appThemeColor,
                          color: Colors.black,
                          text1FontSize: 0.013.toResponsive(context),
                          text2FontSize: 0.014.toResponsive(context))),
                  Container(
                    color: Colors.grey.shade200,
                    child: Icon(
                      Icons.notifications,
                      size: 0.020.toResponsive(context),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 0.005.h(context),
            ),
            Padding(
              padding: bodyLeftRightPadding(context),
              child: Text(
                DateFormat("EEE, MMM dd").format(DateTime.now()),
                style: const TextStyle(fontFamily: "Roboto"),
              ),
            ),

            ///homepage banner widget
            SizedBox(
              height: 0.020.h(context),
            ),
            const HomePageBannerSlider(),

            ///Categories gird widget
            SizedBox(
              height: globalCtrl.devOrient(context) == Orientation.portrait
                  ? 0.002.h(context)
                  : 0.055.h(context),
            ),
            Padding(
              padding: bodyLeftRightPadding(context),
              child: const HomePageCategoriesWidget(),
            ),

            //Special Promos
            SizedBox(
              height: (profCatCtrl.topProfessionalData.value?.results ?? [])
                      .isNotEmpty
                  ? 0.020.h(context)
                  : 0.0,
            ),
            Obx(
              () => (profCatCtrl.topProfessionalData.value?.results ?? [])
                      .isNotEmpty
                  ? Padding(
                      padding: bodyLeftRightPadding(context),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            TextConst.topProf,
                            style: TextStyle(
                                fontSize: 0.016.toResponsive(context),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Roboto"),
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.toNamed(MeroRoutesName.topProfessionalsPage);
                            },
                            child: Text(
                              TextConst.viewAll,
                              style: TextStyle(
                                  fontSize: 0.012.toResponsive(context),
                                  fontWeight: FontWeight.w400,
                                  color: AppColors.appThemeColor,
                                  fontFamily: "Roboto"),
                            ),
                          ),
                        ],
                      ),
                    )
                  : const SizedBox(),
            ),

            Obx(
              () => SizedBox(
                height: (profCatCtrl.topProfessionalData.value?.results ?? [])
                        .isNotEmpty
                    ? 0.015.h(context)
                    : 0.0,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 0.040.w(context)),
              child: const TopProfessionalsWidget(),
            ),

            SizedBox(
              height: 0.030.h(context),
            ),
            Padding(
              padding: bodyLeftRightPadding(context),
              child: HomePageBannerWidget(
                height: globalCtrl.devOrient(context) == Orientation.portrait
                    ? 0.15.h(context)
                    : 0.45.h(context),
                image:
                    "https://www.oliverwyman.com/content/dam/oliver-wyman/v2/publications/2018/july/Automakers-And-Other-Manufacturers-1600x602.jpg.imgix.banner.jpg",
              ),
            ),
            SizedBox(
              height: 0.010.h(context),
            ),
          ],
        ),
      ),
    );
  }
}
