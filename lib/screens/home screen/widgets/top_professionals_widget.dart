import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/fade%20in%20image/fade_in_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

class TopProfessionalsWidget extends StatelessWidget {
  const TopProfessionalsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => profCatCtrl.isLoading2.value
        ? _loadingShimmer(context)
        : SizedBox(
            height: 0.15.h(context),
            width: double.infinity,
            child: GridView.builder(
                //shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: (profCatCtrl
                                .topProfessionalData.value?.results?.length ??
                            0) >
                        10
                    ? 10
                    : profCatCtrl.topProfessionalData.value?.results?.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1, mainAxisSpacing: 15),
                itemBuilder: (ctx, i) {
                  var data = profCatCtrl.topProfessionalData.value?.results?[i];
                  return InkWell(
                    onTap: () {
                      Get.toNamed(MeroRoutesName.professionalsDetailPage,
                          arguments: [
                            globalCtrl.concatName(
                                firstName: data?.user?.firstName ?? "",
                                middleName: data?.user?.middleName,
                                lastName: data?.user?.lastName ?? ""),
                            data?.dob,
                            data?.profession,
                            "${data?.address?.country}, ${data?.address?.city}, ${data?.address?.street}",
                            data?.profilePic,
                            data?.user?.id,
                          ]);
                    },
                    child: Container(
                      height: 0.15.h(context),
                      width: 0.2.w(context),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade300,
                          borderRadius: BorderRadius.circular(8.0)),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(5.0),
                        child: FadedImageWidget(
                          imageUrl: data?.profilePic ?? "",
                          imagePlaceHolder: AssetConst.bLogo,
                          imageError: AssetConst.bLogo,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  );
                }),
          ));
  }

  _loadingShimmer(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey.shade200,
      highlightColor: Colors.white,
      child: Container(
        height: 0.15.h(context),
        width: 0.3.w(context),
        decoration: BoxDecoration(
            color: Colors.blue.shade100,
            borderRadius: BorderRadius.circular(8.0)),
      ),
    );
  }
}
