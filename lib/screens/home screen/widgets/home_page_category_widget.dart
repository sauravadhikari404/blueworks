import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/fade%20in%20image/fade_in_image_widget.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

import '../../../constants/image_assets.dart';

class HomePageCategoriesWidget extends StatelessWidget {
  const HomePageCategoriesWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => profCatCtrl.isLoading.value
          ? _shimmerLoading(context)
          : profCatCtrl.professionalCategoryList.isEmpty
              ? const Center(
                  child: Text(
                    "No categories list to show",
                    style: TextStyle(fontFamily: "Roboto"),
                  ),
                )
              : GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: homePageCtrl.userProfileData.value?.grids,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 5),
                  itemBuilder: (ctx, i) {
                    var data = profCatCtrl.professionalCategoryList[i];
                    return Column(
                      children: [
                        InkWell(
                          onTap: () {
                            Get.toNamed(MeroRoutesName.professionListingPage,
                                arguments: [data, data.categoryId]);
                          },
                          child: Container(
                              height: globalCtrl.devOrient(context) ==
                                      Orientation.landscape
                                  ? 0.2.h(context)
                                  : 0.060.h(context),
                              width: 0.18.w(context),
                              decoration: BoxDecoration(
                                  color: Colors.blue.shade100,
                                  borderRadius: BorderRadius.circular(8.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: FadedImageWidget(
                                  imageUrl: "${data.icon}".simpleMedia(),
                                  imagePlaceHolder: AssetConst.bLogo,
                                  imageError: AssetConst.bLogo,
                                  fit: BoxFit.contain,
                                ),
                              )),
                        ),
                        SizedBox(
                          height: 0.005.h(context),
                        ),
                        Flexible(
                          child: Text(
                            "${data.name?.replaceAll("-", " ")}",
                            style: TextStyle(
                                fontSize: globalCtrl.devOrient(context) ==
                                        Orientation.portrait
                                    ? 0.009.toResponsive(context)
                                    : 0.012.toResponsive(context),
                                color: Colors.black45,
                                fontFamily: "Roboto"),
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ],
                    );
                  }),
    );
  }
}

Widget _shimmerLoading(BuildContext context) {
  return GridView.builder(
      itemCount: 11,
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 4,
        crossAxisSpacing: 10,
      ),
      itemBuilder: (ctx, i) {
        return Column(
          children: [
            Shimmer.fromColors(
              baseColor: Colors.grey.shade200,
              highlightColor: Colors.white,
              child: Container(
                height: 0.060.h(context),
                width: 0.18.w(context),
                decoration: BoxDecoration(
                    color: Colors.blue.shade100,
                    borderRadius: BorderRadius.circular(8.0)),
              ),
            ),
            SizedBox(
              height: 0.005.h(context),
            ),
            Shimmer.fromColors(
              baseColor: Colors.grey.shade200,
              highlightColor: Colors.white,
              child: SizedBox(
                height: 0.010.h(context),
                width: 0.18.w(context),
              ),
            )
          ],
        );
      });
}
