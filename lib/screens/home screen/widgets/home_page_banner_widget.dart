import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/widgets/fade%20in%20image/fade_in_image_widget.dart';
import 'package:flutter/material.dart';

class HomePageBannerWidget extends StatelessWidget {
  const HomePageBannerWidget({super.key, required this.image, this.height});
  final String image;
  final double? height;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: height,
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(8.0)),
      child: FadedImageWidget(
        imageUrl: image,
        imagePlaceHolder: AssetConst.placeHolder,
        imageError: AssetConst.placeHolder,
        fit: BoxFit.cover,
      ),
    );
  }
}
