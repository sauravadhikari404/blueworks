import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/fade%20in%20image/fade_in_image_widget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shimmer/shimmer.dart';

class HomePageBannerSlider extends StatelessWidget {
  const HomePageBannerSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => profCatCtrl.isLoading1.value
          ? Shimmer.fromColors(
              baseColor: Colors.grey.shade200,
              highlightColor: Colors.white,
              child: Container(
                height: 0.17.h(context),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.blue.shade100,
                    borderRadius: BorderRadius.circular(8.0)),
              ),
            )
          : SizedBox(
              height: globalCtrl.devOrient(context) == Orientation.landscape
                  ? 0.45.h(context)
                  : 0.17.h(context),
              width: double.infinity,
              child: CarouselSlider(
                items: profCatCtrl.slideShowList
                    .map((e) => Stack(
                          fit: StackFit.expand,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 0.02.w(context)),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(5),
                                child: FadedImageWidget(
                                  imageUrl: "${e.image}".simpleMedia(),
                                  imagePlaceHolder: AssetConst.placeHolder,
                                  imageError: AssetConst.placeHolder,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          ],
                        ))
                    .toList(),
                options: CarouselOptions(
                  viewportFraction: 0.8,
                  autoPlay: true,
                  autoPlayAnimationDuration: const Duration(milliseconds: 800),
                ),
              ),
            ),
    );
  }
}
