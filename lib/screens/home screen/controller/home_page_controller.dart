import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/models/user_profile_model.dart';
import 'package:blue_works/services/services.dart';
import 'package:get/get.dart';

class HomePageController extends GetxController {
  static HomePageController instance = Get.find();
  Rx<UserProfileModel?> userProfileData = Rx(UserProfileModel());
  RxBool isLoading = true.obs;

  ///This function is used to fetch the logged in user details like name, image, dob etc...
  Future<void> fetchUserProfile() async {
    try {
      isLoading(true);
      userProfileData.value = await Services.fetchUserData();
    } finally {
      isLoading(false);
    }
  }

  @override
  void onInit() {
    fetchUserProfile();
    ever(userProfileData, (value) {
      if (value?.grids != null && value?.user?.id != null) {
        profCatCtrl.fetchProfessionalCategoryModel();
      }
    });
    super.onInit();
  }
}
