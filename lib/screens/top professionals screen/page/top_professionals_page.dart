import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/enum.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:blue_works/widgets/app%20bar/mero_app_bar.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../widgets/custom loading spinner/loading_spinner.dart';
import '../../../widgets/professionals widgets/professional_listing_page_design_widget.dart';
import '../../../widgets/rich text/rich_text.dart';
import '../../../widgets/text form field/search_text_form.dart';

class TopProfessionalsPage extends StatefulWidget {
  const TopProfessionalsPage({super.key});

  @override
  State<TopProfessionalsPage> createState() => _TopProfessionalsPageState();
}

class _TopProfessionalsPageState extends State<TopProfessionalsPage> {
  final ScrollController _scrollController = ScrollController();
  TextEditingController searchTxt = TextEditingController();
  double latitude = 0.0;
  double longitude = 0.0;

  @override
  void initState() {
    _scrollController.addListener(pagination);
    super.initState();
  }

  void pagination() {
    try {
      if ((_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent)) {
        if (((profCatCtrl.topProfessionalData.value?.pagination?.totalPages ??
                0) >
            (profCatCtrl.topProfPage))) {
          setState(() {
            profCatCtrl.topProfPage = profCatCtrl.topProfPage + 1;
            profCatCtrl
                .fetchTopProfessionals(
                    page: profCatCtrl.topProfPage,
                    isRefresh: true,
                    searchTxt: searchTxt.text)
                .then((value) => null)
                .whenComplete(
                    () => profCatCtrl.isLoadingMoreData.value = false);
          });
        }
      }
    } catch (e) {
      DialogueBoxes.flutterToastDialogueBox("Error occurred",
          color: AppColors.errorColor);
      rethrow;
    }
  }

  @override
  void dispose() {
    globalCtrl.timer?.cancel();
    profCatCtrl.topProfPage = 1;
    profCatCtrl.toggleNearBy.value = false;
    searchTxt.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        globalCtrl.unFocusKeybaord(context);
      },
      child: Scaffold(
          appBar: MeroAppBar(
            statusBarColor: AppColors.appThemeColor,
            title: TextConst.topProf,
            leading: IconButton(
                onPressed: () {
                  Get.back();
                },
                icon: const Icon(Icons.arrow_back_ios_new)),
          ),
          body: Column(
            children: [
              SizedBox(
                height: 0.02.h(context),
              ),
              Padding(
                padding: bodyLeftRightPadding(context),
                child: RichTextStyle.richTextWidget(
                    context: context,
                    text1: "Note : ",
                    text1FontSize: 0.012.toResponsive(context),
                    text2FontSize: 0.011.toResponsive(context),
                    color1: Colors.black,
                    color: Colors.black54,
                    fontFamily: "Roboto",
                    text2:
                        "Tap on the first icon inside search field to get near by professions"),
              ),
              SizedBox(
                height: 0.02.h(context),
              ),
              Padding(
                padding: bodyLeftRightPadding(context),
                child: SearchTextFormField(
                  title: "Search by name or address",
                  suffixIcon: const Icon(
                    Icons.search,
                    color: Colors.black38,
                  ),
                  prefixIcon: nearMeIcon(),
                  controller: searchTxt,
                  whichPage: WHICHPAGE.topProfessionalPage,
                ),
              ),
              Obx(
                () => profCatCtrl.isLoading2.value
                    ? Center(
                        child: LoadingSpinner.showLoadingSpinner(
                            context: context,
                            text: "Fetching top professionals"))
                    : (profCatCtrl.topProfessionalData.value?.results ?? [])
                            .isEmpty
                        ? Center(
                            child: Text(
                              "No data to show !",
                              style: TextStyle(
                                  fontSize: 0.014.toResponsive(context)),
                            ),
                          )
                        : RefreshIndicator(
                            onRefresh: () async {
                              profCatCtrl.fetchTopProfessionals(
                                  page: 1,
                                  searchTxt: searchTxt.text,
                                  latitude: latitude,
                                  longitude: longitude);
                            },
                            child: SingleChildScrollView(
                              physics: const ClampingScrollPhysics(
                                  parent: AlwaysScrollableScrollPhysics()),
                              child: Column(
                                children: [
                                  ListView.builder(
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: profCatCtrl.topProfessionalData
                                          .value?.results?.length,
                                      itemBuilder: (ctx, i) {
                                        var data = profCatCtrl
                                            .topProfessionalData
                                            .value
                                            ?.results?[i];
                                        return GestureDetector(
                                          onTap: () {
                                            Get.toNamed(
                                                MeroRoutesName
                                                    .professionalsDetailPage,
                                                arguments: [
                                                  globalCtrl.concatName(
                                                      firstName: data?.user
                                                              ?.firstName ??
                                                          "",
                                                      middleName: data
                                                          ?.user?.middleName,
                                                      lastName: data?.user
                                                              ?.lastName ??
                                                          ""),
                                                  data?.dob,
                                                  data?.profession,
                                                  "${data?.address?.country}, ${data?.address?.city}, ${data?.address?.street}",
                                                  data?.profilePic,
                                                  data?.user?.id,
                                                ]);
                                          },
                                          child: Padding(
                                            padding: bodyOnlyPadding(context),
                                            child:
                                                ProfessionalListingDesignWidget(
                                              profilePic:
                                                  data?.profilePic ?? "",
                                              name: globalCtrl.concatName(
                                                  firstName:
                                                      data?.user?.firstName ??
                                                          "",
                                                  middleName:
                                                      data?.user?.middleName,
                                                  lastName:
                                                      data?.user?.lastName ??
                                                          ""),
                                              profession: data?.profession,
                                              ratingCount: data?.rating,
                                            ),
                                          ),
                                        );
                                      }),
                                  profCatCtrl.isLoadingMoreData.value
                                      ? Center(
                                          child: CircularProgressIndicator(
                                            color: AppColors.appThemeColor,
                                          ),
                                        )
                                      : const SizedBox()
                                ],
                              ),
                            ),
                          ),
              ),
            ],
          )),
    );
  }

  Widget nearMeIcon() {
    return GestureDetector(
      onTap: () async {
        if (networkCtrl.connectionType.value == 1 ||
            networkCtrl.connectionType.value == 2) {
          profCatCtrl.toggleNearBy.toggle();
          if (profCatCtrl.toggleNearBy.isTrue) {
            profCatCtrl.isLoading2(true);
            var data = await globalCtrl.determinePosition();
            latitude = data.latitude;
            longitude = data.longitude;
            profCatCtrl.fetchTopProfessionals(
                page: profCatCtrl.topProfPage,
                searchTxt: searchTxt.text,
                latitude: latitude,
                longitude: longitude);
          }
        } else if (networkCtrl.connectionType.value == 0) {
          DialogueBoxes.flutterToastDialogueBox(
              "Please check you internet connection",
              color: AppColors.errorColor);
        }
      },
      child: Obx(
        () => Icon(
          Icons.near_me,
          color: profCatCtrl.toggleNearBy.value == true
              ? AppColors.appThemeColor
              : Colors.black54,
        ),
      ),
    );
  }
}
