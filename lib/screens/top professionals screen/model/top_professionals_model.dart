import 'dart:convert';

import 'package:blue_works/models/pagination_model.dart';

TopProfessionalsModel topProfessionalsModelFromJson(String str) =>
    TopProfessionalsModel.fromJson(json.decode(str));

String topProfessionalsModelToJson(TopProfessionalsModel data) =>
    json.encode(data.toJson());

class TopProfessionalsModel {
  TopProfessionalsModel({
    this.pagination,
    this.results,
  });

  Pagination? pagination;
  List<Result>? results;

  factory TopProfessionalsModel.fromJson(Map<String, dynamic> json) =>
      TopProfessionalsModel(
        pagination: Pagination.fromJson(json["pagination"]),
        results:
            List<Result>.from(json["results"].map((x) => Result.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "pagination": pagination?.toJson(),
        "results": List<dynamic>.from(results?.map((x) => x.toJson()) ?? []),
      };
}

class Result {
  Result({
    this.id,
    this.bio,
    this.dob,
    this.user,
    this.address,
    this.profession,
    this.rating,
    this.profilePic,
    this.grids,
  });

  int? id;
  String? bio;
  DateTime? dob;
  User? user;
  Address? address;
  List<String>? profession;
  double? rating;
  String? profilePic;
  int? grids;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        id: json["id"],
        bio: json["bio"],
        dob: DateTime.parse(json["dob"]),
        user: User.fromJson(json["user"]),
        address: Address.fromJson(json["address"]),
        profession: List<String>.from(json["profession"].map((x) => x)),
        rating: json["rating"],
        profilePic: json["profile_pic"],
        grids: json["grids"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "bio": bio,
        "dob":
            "${dob?.year.toString().padLeft(4, '0')}-${dob?.month.toString().padLeft(2, '0')}-${dob?.day.toString().padLeft(2, '0')}",
        "user": user?.toJson(),
        "address": address?.toJson(),
        "profession": List<dynamic>.from(profession?.map((x) => x) ?? []),
        "rating": rating,
        "profile_pic": profilePic,
        "grids": grids,
      };
}

class Address {
  Address({
    this.city,
    this.country,
    this.street,
  });

  String? city;
  String? country;
  String? street;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        city: json["city"],
        country: json["country"],
        street: json["street"],
      );

  Map<String, dynamic> toJson() => {
        "city": city,
        "country": country,
        "street": street,
      };
}

class User {
  User({
    this.id,
    this.firstName,
    this.middleName,
    this.lastName,
  });

  int? id;
  String? firstName;
  String? middleName;
  String? lastName;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        firstName: json["first_name"],
        middleName: json["middle_name"],
        lastName: json["last_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "first_name": firstName,
        "middle_name": middleName,
        "last_name": lastName,
      };
}
