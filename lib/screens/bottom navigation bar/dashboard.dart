import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/controller/professional_category_controller.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/screens/home%20screen/controller/home_page_controller.dart';
import 'package:blue_works/screens/home%20screen/pages/home_page.dart';
import 'package:blue_works/screens/setting%20screen/pages/settings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../constants/texts.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({super.key});

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  int _currentIndex = 0;

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = const HomePage();

  @override
  void initState() {
    Get.put(HomePageController());
    Get.put(ProfessionalCategoryController());
    _checkInternetConnection();
    super.initState();
  }

  _checkInternetConnection() {
    if (networkCtrl.connectionType.value == 1 ||
        networkCtrl.connectionType.value == 2) {
      homePageCtrl.fetchUserProfile();
      profCatCtrl.fetchTopProfessionals(page: 1);
    } else if (networkCtrl.connectionType.value == 0) {
      Get.toNamed(MeroRoutesName.noInternetPage);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        bucket: bucket,
        child: currentScreen,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: SizedBox(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              ///left tab
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = const HomePage();
                        _currentIndex = 0;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.home,
                          color: _currentIndex == 0
                              ? AppColors.appThemeColor
                              : Colors.grey,
                        ),
                        Text(
                          TextConst.home,
                          style: TextStyle(
                              color: _currentIndex == 0
                                  ? AppColors.appThemeColor
                                  : Colors.grey,
                              fontFamily: "Roboto"),
                        )
                      ],
                    ),
                  )
                ],
              ),

              ///Right Tab
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  MaterialButton(
                    minWidth: 40,
                    onPressed: () {
                      setState(() {
                        currentScreen = const SettingPage();
                        _currentIndex = 1;
                      });
                    },
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.settings,
                          color: _currentIndex == 1
                              ? AppColors.appThemeColor
                              : Colors.grey,
                        ),
                        Text(
                          TextConst.setting,
                          style: TextStyle(
                              color: _currentIndex == 1
                                  ? AppColors.appThemeColor
                                  : Colors.grey,
                              fontFamily: "Roboto"),
                        )
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
