import 'package:flutter/material.dart';

class ElevatedButtonWiddget extends StatelessWidget {
  const ElevatedButtonWiddget(
      {super.key,
      this.text,
      this.backgroundColor,
      this.tappedFunction,
      this.textColor,
      this.textFontSize,
      this.fontFamily});
  final String? text;
  final Color? backgroundColor;
  final VoidCallback? tappedFunction;
  final Color? textColor;
  final double? textFontSize;
  final String? fontFamily;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: backgroundColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
      ),
      onPressed: tappedFunction,
      child: Text(
        "$text",
        style: TextStyle(
            color: textColor, fontSize: textFontSize, fontFamily: fontFamily),
      ),
    );
  }
}
