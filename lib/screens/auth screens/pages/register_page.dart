import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/screens/auth%20screens/widgets/button.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/rich%20text/rich_text.dart';
import 'package:blue_works/widgets/text%20form%20field/text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../route/route_constants.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  void initState() {
    super.initState();
    globalCtrl.setStatusBarColor(colorss: AppColors.appThemeColor);
  }

  @override
  void dispose() {
    globalCtrl.removeStatusBarColor();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => globalCtrl.unFocusKeybaord(context),
      child: Scaffold(
        backgroundColor: AppColors.bodyColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(AssetConst.createAccountPng),
              Padding(
                padding: bodyPadding(context),
                child: Form(
                    key: authCtrl.registerFormKey,
                    child: Column(
                      children: [
                        TextFormFieldWidget(
                          title: TextConst.fName,
                          inputType: TextInputType.name,
                          prefixIcon: const Icon(Icons.person),
                          controller: authCtrl.firstNameCtrl,
                          txtCapital: TextCapitalization.words,
                        ),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                        _middleNameTextField(context),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                        TextFormFieldWidget(
                          title: TextConst.lName,
                          inputType: TextInputType.name,
                          prefixIcon: const Icon(Icons.person),
                          controller: authCtrl.lastNameCtrl,
                          txtCapital: TextCapitalization.words,
                        ),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                        TextFormFieldWidget(
                          title: TextConst.phone,
                          inputType: TextInputType.phone,
                          prefixIcon: Padding(
                            padding: EdgeInsets.only(
                                top: 0.016.h(context), left: 0.010.w(context)),
                            child: const Text(
                              "+977",
                              style: TextStyle(color: Colors.black38),
                            ),
                          ),
                          controller: authCtrl.phoneCtrl,
                        ),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                        Obx(
                          () => TextFormFieldWidget(
                            title: TextConst.password,
                            inputType: TextInputType.text,
                            obsecure: authCtrl.registerPasswordObsecure.value,
                            prefixIcon: const Icon(Icons.password),
                            suffixIcon: IconButton(
                                onPressed: () {
                                  authCtrl.registerPasswordObsecure.toggle();
                                },
                                icon: authCtrl.registerPasswordObsecure.value
                                    ? const Icon(Icons.remove_red_eye_outlined)
                                    : const Icon(Icons.visibility_off)),
                            controller: authCtrl.password1Ctrl,
                          ),
                        ),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                        Obx(
                          () => TextFormFieldWidget(
                            title: TextConst.confirmPass,
                            inputType: TextInputType.text,
                            obsecure:
                                authCtrl.registerConfirmPassObsecure.value,
                            prefixIcon: const Icon(Icons.password),
                            suffixIcon: IconButton(
                                onPressed: () {
                                  authCtrl.registerConfirmPassObsecure.toggle();
                                },
                                icon: authCtrl.registerConfirmPassObsecure.value
                                    ? const Icon(Icons.remove_red_eye_outlined)
                                    : const Icon(Icons.visibility_off)),
                            controller: authCtrl.password2Ctrl,
                          ),
                        ),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                        SizedBox(
                          width: double.infinity,
                          height: 0.055.h(context),
                          child: ElevatedButtonWiddget(
                            text: TextConst.signUp,
                            fontFamily: "Roboto",
                            textColor: Colors.white,
                            textFontSize: 0.014.toResponsive(context),
                            backgroundColor: AppColors.appThemeColor,
                            tappedFunction: () {
                              if (authCtrl.registerFormKey.currentState!
                                          .validate() &&
                                      networkCtrl.connectionType.value == 1 ||
                                  networkCtrl.connectionType.value == 2) {
                                authCtrl.validateRegister();
                              } else if (networkCtrl.connectionType.value ==
                                  0) {
                                DialogueBoxes.flutterToastDialogueBox(
                                    "Please check you internet connection",
                                    color: AppColors.errorColor);
                              }
                            },
                          ),
                        ),
                        SizedBox(
                          height: 0.030.h(context),
                        ),
                        RichTextStyle.richTextWidget(
                            text1: TextConst.alreadyAccount,
                            text2: TextConst.login,
                            fontFamily: "Roboto",
                            context: context,
                            color: AppColors.appThemeColor,
                            tapFunction: () {
                              Get.offAllNamed(MeroRoutesName.loginPage);
                              authCtrl.clearTextFormFields();
                              authCtrl.registerPasswordObsecure(true);
                              authCtrl.registerConfirmPassObsecure(true);
                            },
                            txtDecoration: TextDecoration.underline),
                        SizedBox(
                          height: 0.020.h(context),
                        ),
                      ],
                    )),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _middleNameTextField(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
          fillColor: Colors.white,
          filled: true,
          prefixIcon: const Icon(Icons.person),
          hintText: TextConst.mName,
          hintStyle: TextStyle(
              fontSize: 0.012.toResponsive(context), color: Colors.black26)),
      controller: authCtrl.middleNameCtrl,
      textCapitalization: TextCapitalization.words,
    );
  }
}
