import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/screens/auth%20screens/widgets/button.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/rich%20text/rich_text.dart';
import 'package:blue_works/widgets/text%20form%20field/text_form_field.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    super.initState();
    globalCtrl.setStatusBarColor(colorss: AppColors.appThemeColor);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => globalCtrl.unFocusKeybaord(context),
      child: Scaffold(
        backgroundColor: AppColors.bodyColor,
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                child: Image.asset(
                  AssetConst.loginPng,
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                height: 0.020.h(context),
              ),
              Padding(
                padding: bodyPadding(context),
                child: Form(
                    key: authCtrl.loginFormKey,
                    child: Column(
                      children: [
                        TextFormFieldWidget(
                          title: TextConst.phoneOrEmail,
                          controller: authCtrl.emailOrPhoneCtrl,
                        ),
                        SizedBox(height: 0.020.h(context)),
                        Obx(
                          () => TextFormFieldWidget(
                            title: TextConst.password,
                            obsecure: authCtrl.loginPasswordObsecure.value,
                            suffixIcon: IconButton(
                                onPressed: () {
                                  authCtrl.loginPasswordObsecure.toggle();
                                },
                                icon: authCtrl.loginPasswordObsecure.value
                                    ? const Icon(Icons.remove_red_eye_outlined)
                                    : const Icon(Icons.visibility_off)),
                            controller: authCtrl.passwordCtrl,
                          ),
                        ),
                      ],
                    )),
              ),
              SizedBox(
                height: 0.020.h(context),
              ),
              Padding(
                padding: bodyLeftRightPadding(context),
                child: SizedBox(
                  width: double.infinity,
                  height: 0.055.h(context),
                  child: ElevatedButtonWiddget(
                    text: TextConst.login,
                    fontFamily: "Roboto",
                    textFontSize: 0.014.toResponsive(context),
                    textColor: Colors.white,
                    tappedFunction: () {
                      if (authCtrl.loginFormKey.currentState!.validate() &&
                              networkCtrl.connectionType.value == 1 ||
                          networkCtrl.connectionType.value == 2) {
                        authCtrl.loginUser(
                            emailOrPhone: authCtrl.emailOrPhoneCtrl.text,
                            password: authCtrl.passwordCtrl.text);
                      } else if (networkCtrl.connectionType.value == 0) {
                        DialogueBoxes.flutterToastDialogueBox(
                            TextConst.noInternetTxt,
                            color: AppColors.errorColor);
                      }
                    },
                    backgroundColor: AppColors.appThemeColor,
                  ),
                ),
              ),
              SizedBox(
                height: 0.030.h(context),
              ),
              RichTextStyle.richTextWidget(
                  text1: TextConst.noAccount,
                  text2: TextConst.signUp,
                  fontFamily: "Roboto",
                  context: context,
                  color: AppColors.appThemeColor,
                  tapFunction: () {
                    Get.offAllNamed(MeroRoutesName.registerPage);
                    authCtrl.clearTextFormFields();
                    authCtrl.loginPasswordObsecure(true);
                  },
                  txtDecoration: TextDecoration.underline)
            ],
          ),
        ),
      ),
    );
  }
}
