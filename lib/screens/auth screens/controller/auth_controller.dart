import 'package:blue_works/constants/color.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/local%20storage/local_storage.dart';
import 'package:blue_works/services/services.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  static AuthController instance = Get.find();
  RxBool obsecure = false.obs;
  RxBool isLoding = false.obs;
  TextEditingController emailOrPhoneCtrl = TextEditingController();
  TextEditingController passwordCtrl = TextEditingController();

  TextEditingController firstNameCtrl = TextEditingController();
  TextEditingController middleNameCtrl = TextEditingController();
  TextEditingController lastNameCtrl = TextEditingController();
  TextEditingController emailCtrl = TextEditingController();
  TextEditingController phoneCtrl = TextEditingController();
  TextEditingController password1Ctrl = TextEditingController();
  TextEditingController password2Ctrl = TextEditingController();
  final loginFormKey = GlobalKey<FormState>();
  final registerFormKey = GlobalKey<FormState>();
  RxBool loginPasswordObsecure = true.obs;
  RxBool registerPasswordObsecure = true.obs;
  RxBool registerConfirmPassObsecure = true.obs;

  ///This function is used to login user
  Future<void> loginUser(
      {required String emailOrPhone, required String password}) async {
    try {
      DialogueBoxes.showLoadingDialogue();
      await Services.loginUser(emailOrPhone: emailOrPhone, password: password);
    } finally {
      DialogueBoxes.dismissLoadingDialog();
    }
  }

  ///This function is used to register user
  Future<void> registerUser(
      {required String phone,
      required String password,
      required firstName,
      String? middleName,
      String? lastName,
      String? role}) async {
    try {
      DialogueBoxes.showLoadingDialogue();
      await Services.userRegister(
          phone: phone,
          password: password,
          firstName: firstName,
          middleName: middleName,
          lastName: lastName,
          role: role);
    } finally {
      DialogueBoxes.dismissLoadingDialog();
    }
  }

  ///This is used to validate the login call function which we have written above
  validateRegister() {
    String firstName = firstNameCtrl.text;
    String middleName = middleNameCtrl.text;
    String lastName = lastNameCtrl.text;
    String phone = phoneCtrl.text;
    String password1 = password1Ctrl.text;
    String password2 = password2Ctrl.text;

    if (firstName.isEmpty ||
        lastName.isEmpty ||
        phone.isEmpty ||
        password1.isEmpty ||
        password2.isEmpty) {
      DialogueBoxes.flutterToastDialogueBox("Please give more information",
          color: AppColors.errorColor);
    } else if (password1 != password2) {
      DialogueBoxes.flutterToastDialogueBox("password does not match",
          color: AppColors.errorColor);
    } else {
      registerUser(
          phone: phone,
          password: password1,
          firstName: firstName,
          middleName: middleName,
          lastName: lastName,
          role: "");
    }
  }

  clearTextFormFields() {
    emailOrPhoneCtrl.text = "";
    passwordCtrl.text = "";
    firstNameCtrl.text = "";
    middleNameCtrl.text = "";
    lastNameCtrl.text = "";
    emailCtrl.text = "";
    phoneCtrl.text = "";
    password1Ctrl.text = "";
    password2Ctrl.text = "";
  }

  void logoutUser() {
    debugPrint("logout called");
    LocalStorage().clearCredential();
    Get.offAndToNamed(MeroRoutesName.loginPage);
  }
}
