import 'dart:io';

import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/padding.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/screens/profile/controller/edit_profile_controller.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/fade%20in%20image/fade_in_image_widget.dart';
import 'package:blue_works/widgets/app%20bar/mero_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/controllers.dart';
import '../../../constants/image_assets.dart';
import '../../../widgets/dialogue boxes/dialogue_boxes.dart';
import '../../../widgets/text form field/text_form_field.dart';
import '../../auth screens/widgets/button.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({super.key});

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage> {
  @override
  void initState() {
    Get.put(EditProfileController());
    editProfCtrl.firstNameCtrl.text =
        homePageCtrl.userProfileData.value?.user?.firstName ?? "";
    editProfCtrl.middleNameCtrl.text =
        homePageCtrl.userProfileData.value?.user?.middleName ?? "";
    editProfCtrl.lastNameCtrl.text =
        homePageCtrl.userProfileData.value?.user?.lastName ?? "";
    editProfCtrl.dobCtrl.text = homePageCtrl.userProfileData.value?.dob ?? "";

    super.initState();
  }

  @override
  void dispose() {
    globalCtrl.singleCompressedFile?.value = File("");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: MeroAppBar(
        title: TextConst.profile,
        leading: IconButton(
            onPressed: () {
              Get.back();
            },
            icon: const Icon(Icons.arrow_back_ios_new)),
      ),
      body: Padding(
        padding: bodyPadding(context),
        child: SingleChildScrollView(
          child: Column(
            children: [
              _image(context),
              const Text(
                TextConst.changePP,
                style: TextStyle(fontFamily: "Roboto"),
              ),
              SizedBox(
                height: 0.020.h(context),
              ),
              Form(
                  key: authCtrl.registerFormKey,
                  child: Column(
                    children: [
                      TextFormFieldWidget(
                        title: TextConst.fName,
                        inputType: TextInputType.name,
                        prefixIcon: const Icon(Icons.person),
                        controller: editProfCtrl.firstNameCtrl,
                      ),
                      SizedBox(
                        height: 0.020.h(context),
                      ),
                      _middleNameTextField(context),
                      SizedBox(
                        height: 0.020.h(context),
                      ),
                      TextFormFieldWidget(
                        title: TextConst.lName,
                        inputType: TextInputType.name,
                        prefixIcon: const Icon(Icons.person),
                        controller: editProfCtrl.lastNameCtrl,
                      ),
                      SizedBox(
                        height: 0.020.h(context),
                      ),
                      TextFormFieldWidget(
                        title: TextConst.dob,
                        inputType: TextInputType.phone,
                        prefixIcon: const Icon(Icons.cake),
                        controller: editProfCtrl.dobCtrl,
                      ),
                      SizedBox(
                        height: 0.020.h(context),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 0.055.h(context),
                        child: ElevatedButtonWiddget(
                          text: TextConst.edit,
                          fontFamily: "Roboto",
                          textColor: Colors.white,
                          textFontSize: 0.014.toResponsive(context),
                          backgroundColor: AppColors.appThemeColor,
                          tappedFunction: () {
                            if (authCtrl.registerFormKey.currentState!
                                        .validate() &&
                                    networkCtrl.connectionType.value == 1 ||
                                networkCtrl.connectionType.value == 2) {
                              ///api call here
                            } else if (networkCtrl.connectionType.value == 0) {
                              DialogueBoxes.flutterToastDialogueBox(
                                  "Please check you internet connection",
                                  color: AppColors.errorColor);
                            }
                          },
                        ),
                      ),
                      SizedBox(
                        height: 0.030.h(context),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

Widget _middleNameTextField(BuildContext context) {
  return TextFormField(
    decoration: InputDecoration(
        border: const OutlineInputBorder(
          borderSide: BorderSide.none,
        ),
        fillColor: Colors.white,
        filled: true,
        prefixIcon: const Icon(Icons.person),
        hintText: TextConst.mName,
        hintStyle: TextStyle(
            fontSize: 0.012.toResponsive(context), color: Colors.black26)),
    controller: editProfCtrl.middleNameCtrl,
    textCapitalization: TextCapitalization.words,
  );
}

Widget _image(BuildContext context) {
  return Stack(
    children: [
      Align(
        alignment: Alignment.center,
        child: Obx(
          () => SizedBox(
            height: 0.2.h(context),
            width: 0.38.w(context),
            child: AspectRatio(
              aspectRatio: 1 / 1,
              child: ClipOval(
                child: globalCtrl.singleCompressedFile?.value == null ||
                        (globalCtrl.singleCompressedFile?.value.path ?? "")
                            .isEmpty
                    ? FadedImageWidget(
                        imageUrl:
                            "${homePageCtrl.userProfileData.value?.profilePic}"
                                .simpleMedia(),
                        imagePlaceHolder: AssetConst.profile,
                        imageError: AssetConst.profile,
                        fit: BoxFit.cover,
                      )
                    : Image.file(
                        File(globalCtrl.singleCompressedFile?.value.path ?? ""),
                        fit: BoxFit.cover,
                      ),
              ),
            ),
          ),
        ),
      ),
      Align(
          alignment: Alignment.center,
          child: Padding(
            padding:
                EdgeInsets.only(top: 0.14.h(context), left: 0.25.w(context)),
            child: Container(
              decoration: BoxDecoration(
                  color: AppColors.appThemeColor, shape: BoxShape.circle),
              child: InkWell(
                onTap: () {
                  globalCtrl.pickSingleImage();
                },
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 0.023.toResponsive(context),
                ),
              ),
            ),
          )),
    ],
  );
}
