import 'package:blue_works/constants/color.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

class DialogueBoxes {
  DialogueBoxes._();
  factory DialogueBoxes() => instance;
  static DialogueBoxes instance = DialogueBoxes._();

  static flutterToastDialogueBox(String message,
      {Color color = Colors.black,
      ToastGravity gravity = ToastGravity.BOTTOM,
      Toast length = Toast.LENGTH_SHORT}) {
    return Fluttertoast.showToast(
        msg: message,
        backgroundColor: color,
        toastLength: length,
        gravity: gravity);
  }

  static showLoadingDialogue() {
    showDialog(
        context: Get.context!,
        builder: (context) {
          return Center(
            child: CircularProgressIndicator(
              color: AppColors.appThemeColor,
            ),
          );
        });
  }

  static dismissLoadingDialog() {
    return Get.back();
  }
}
