import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

import '../../constants/texts.dart';
import '../../local storage/local_storage.dart';
import '../../route/route_constants.dart';
import '../dialogue boxes/dialogue_boxes.dart';

class NoInternetWidget extends StatelessWidget {
  const NoInternetWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          LottieBuilder.asset(
            AssetConst.noInternet,
            repeat: false,
          ),
          SizedBox(
            height: 0.2.h(context),
          ),
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                color: AppColors.appThemeColor,
                borderRadius: BorderRadius.vertical(
                    top: Radius.elliptical(
                        MediaQuery.of(context).size.width, 100.0)),
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "No Internet Connection",
                    style: TextStyle(
                        fontSize: 0.016.toResponsive(context),
                        color: Colors.white,
                        fontFamily: "Roboto",
                        fontWeight: FontWeight.w600),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 0.005.h(context),
                  ),
                  Text(
                    "You are not connected to internet make sure wifi is on,\nAirplane mode is off and try again.",
                    style: TextStyle(
                        fontSize: 0.012.toResponsive(context),
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Roboto"),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 0.04.h(context),
                  ),
                  ElevatedButton(
                      onPressed: () async {
                        if (networkCtrl.connectionType.value == 1 ||
                            networkCtrl.connectionType.value == 2) {
                          final autoLogin =
                              await LocalStorage().getAccessToken();
                          if (autoLogin != null) {
                            Get.offAllNamed(MeroRoutesName.dashBoard);
                          } else if (autoLogin == null) {
                            Get.offAllNamed(MeroRoutesName.loginPage);
                          }
                        } else if (networkCtrl.connectionType.value == 0) {
                          DialogueBoxes.flutterToastDialogueBox(
                              TextConst.noInternetTxt,
                              color: AppColors.errorColor);
                        }
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20),
                        ),
                      ),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 0.03.w(context)),
                        child: Text(
                          "Retry",
                          style: TextStyle(
                              color: AppColors.appThemeColor,
                              fontFamily: "Roboto"),
                        ),
                      ))
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
