import 'package:flutter/material.dart';

import '../../constants/keys.dart';

class FadedImageWidget extends StatelessWidget {
  const FadedImageWidget(
      {required this.imageUrl,
      required this.imagePlaceHolder,
      required this.imageError,
      this.height,
      this.width,
      this.fit,
      super.key});
  final String imageUrl;
  final String imagePlaceHolder;
  final String imageError;
  final BoxFit? fit;
  final double? height;
  final double? width;
  @override
  Widget build(BuildContext context) {
    logger.i("This is url sdasdasda", imageUrl);
    return FadeInImage(
      height: height,
      width: width,
      placeholder: AssetImage(imagePlaceHolder),
      image: NetworkImage(imageUrl),
      imageErrorBuilder: (context, error, stackTrace) => Image.asset(
        imageError,
        fit: fit ?? BoxFit.fill,
      ),
      fit: fit ?? BoxFit.fill,
      placeholderFit: fit ?? BoxFit.fill,
    );
  }
}
