import 'package:blue_works/constants/image_assets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../fade in image/fade_in_image_widget.dart';

class SinglePhotoViewerWidget extends StatelessWidget {
  const SinglePhotoViewerWidget({super.key});
  @override
  Widget build(BuildContext context) {
    String image = Get.arguments;
    return Scaffold(
      body: image == ""
          ? const Center(
              child: Text("No Image to show !"),
            )
          : InteractiveViewer(
              maxScale: 20,
              minScale: 0.5,
              panEnabled: true,
              scaleEnabled: true,
              child: SizedBox(
                  height: double.infinity,
                  width: double.infinity,
                  child: FadedImageWidget(
                    imageUrl: image,
                    imagePlaceHolder: AssetConst.bLogo,
                    imageError: AssetConst.bLogo,
                    fit: BoxFit.fill,
                  )),
            ),
    );
  }
}
