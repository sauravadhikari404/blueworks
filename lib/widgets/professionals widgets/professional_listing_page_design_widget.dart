import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';

import '../../constants/image_assets.dart';
import '../fade in image/fade_in_image_widget.dart';

class ProfessionalListingDesignWidget extends StatelessWidget {
  const ProfessionalListingDesignWidget(
      {super.key,
      this.profilePic,
      this.name,
      this.ratingCount,
      this.address,
      this.profession});
  final String? profilePic;
  final String? name;
  final String? address;
  final List<String>? profession;
  final double? ratingCount;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: Colors.black26),
          borderRadius: BorderRadius.circular(10)),
      child: ListTile(
          dense: true,
          visualDensity: VisualDensity.adaptivePlatformDensity,
          leading: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: FadedImageWidget(
              height: 0.1.h(context),
              width: 0.12.w(context),
              imageUrl: profilePic ?? "",
              imagePlaceHolder: AssetConst.bLogo,
              imageError: AssetConst.profile,
              fit: BoxFit.cover,
            ),
          ),
          isThreeLine: true,
          title: Padding(
            padding: EdgeInsets.symmetric(vertical: 0.005.h(context)),
            child: Text(
              "$name",
              style: TextStyle(
                  fontSize: 0.014.toResponsive(context),
                  fontWeight: FontWeight.w600),
            ),
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                address ??
                    profession
                        .toString()
                        .replaceAll("[", "")
                        .replaceAll("]", ""),
                style: TextStyle(fontSize: 0.012.toResponsive(context)),
              ),
              SizedBox(
                height: 0.005.h(context),
              ),
            ],
          ),
          minVerticalPadding: 5.0),
    );
  }
}
