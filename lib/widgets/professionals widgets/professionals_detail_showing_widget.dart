import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/image_assets.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/constants/texts.dart';
import 'package:blue_works/controller/professionals_detail_page_controller.dart';
import 'package:blue_works/widgets/custom%20loading%20spinner/loading_spinner.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:blue_works/widgets/fade%20in%20image/fade_in_image_widget.dart';
import 'package:blue_works/widgets/rich%20text/rich_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import '../../constants/keys.dart';

class ProfessionalDetailShowingWidget extends StatefulWidget {
  const ProfessionalDetailShowingWidget({
    super.key,
  });

  @override
  State<ProfessionalDetailShowingWidget> createState() =>
      _ProfessionalDetailShowingWidgetState();
}

class _ProfessionalDetailShowingWidgetState
    extends State<ProfessionalDetailShowingWidget>
    with SingleTickerProviderStateMixin {
  final String? name = Get.arguments[0];
  final DateTime? dob = Get.arguments[1];
  final List<String>? profession = Get.arguments[2];
  final String? address = Get.arguments[3];
  final String? image = Get.arguments[4];
  final int? id = Get.arguments[5];
  TabController? ctrl;
  @override
  void initState() {
    super.initState();
    globalCtrl.setStatusBarColor(colorss: AppColors.appThemeColor);
    Get.put(ProfessionalDetailPageController());
    ctrl = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    globalCtrl.removeStatusBarColor();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.bodyColor,
        body: CustomScrollView(
          slivers: [
            SliverAppBar(
              backgroundColor: Colors.transparent,
              leading: IconButton(
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(Icons.arrow_back_ios_new)),
              title: Text(
                TextConst.profDetailPage,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 0.016.toResponsive(context),
                    fontFamily: "Roboto"),
              ),
              expandedHeight: 0.3.h(context),
              flexibleSpace: FlexibleSpaceBar(
                background: Stack(
                  children: [_topContainer(context), _circleImage(context)],
                ),
              ),
            ),
            SliverToBoxAdapter(
              child: Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    Text(
                      name ?? "",
                      style: TextStyle(
                          fontFamily: "Roboto",
                          fontSize: 0.014.toResponsive(context)),
                    ),
                    SizedBox(height: 0.005.h(context)),
                    RichTextStyle.richTextWidget(
                        context: context,
                        text1: "Dob: ",
                        text2: DateFormat("yyyy-MM-dd")
                            .format(dob ?? DateTime.now()),
                        color1: Colors.black,
                        color: Colors.black54,
                        fontFamily: "Roboto"),
                    SizedBox(height: 0.015.h(context)),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        RichTextStyle.richTextWidget(
                            context: context,
                            text1: "Followers:",
                            text2: "10",
                            color1: AppColors.appThemeColor,
                            text1FontSize: 0.014.toResponsive(context),
                            color: Colors.black54,
                            fontFamily: "Roboto"),
                        SizedBox(width: 0.015.h(context)),
                        RichTextStyle.richTextWidget(
                            context: context,
                            text1: "Following:",
                            text2: "10",
                            text1FontSize: 0.014.toResponsive(context),
                            color1: AppColors.successColor,
                            color: Colors.black54,
                            fontFamily: "Roboto"),
                      ],
                    )
                  ],
                ),
              ),
            ),

            // const SliverToBoxAdapter(
            //   child: Divider(
            //     thickness: 0.5,
            //     color: Colors.black38,
            //   ),
            // ),
            SliverFillRemaining(child: _tabBar(context))
          ],
        ));
  }

  Widget _topContainer(BuildContext context) => Container(
        height: 0.25.h(context),
        width: double.infinity,
        decoration: BoxDecoration(
            color: AppColors.appThemeColor,
            borderRadius:
                const BorderRadius.only(bottomLeft: Radius.circular(100))),
      );

  Widget _circleImage(BuildContext context) => Center(
        child: Padding(
          padding: EdgeInsets.only(top: 0.12.h(context)),
          child: SizedBox(
            height: 0.15.h(context),
            width: 0.32.w(context),
            child: AspectRatio(
              aspectRatio: 1 / 1,
              child: ClipOval(
                child: FadedImageWidget(
                  imageUrl: "$image".simpleMedia().simpleMedia(),
                  imagePlaceHolder: AssetConst.profile,
                  imageError: AssetConst.profile,
                  fit: BoxFit.cover,
                ),
              ),
            ),
          ),
        ),
      );

  Widget _tabBar(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(
                  left: 0.02.w(context),
                  right: 0.02.w(context),
                  top: 0.04.h(context)),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8.0)),
                child: TabBar(
                    indicatorColor: AppColors.appThemeColor,
                    unselectedLabelColor: Colors.grey,
                    labelColor: AppColors.appThemeColor,
                    onTap: (i) {
                      if (i == 0) {
                        logger.i("about");
                      } else if (i == 1) {
                        logger.i("folowers");
                      } else {
                        logger.i("gallery");
                        profDetailPageCtrl.fetchGalleryShowCaseList(id: id);
                      }
                    },
                    tabs: const [
                      Tab(
                        icon: Icon(
                          Icons.info_outline,
                          size: 30,
                        ),
                      ),
                      Tab(
                        icon: Icon(
                          Icons.local_activity_outlined,
                          size: 30,
                        ),
                      ),
                      Tab(
                          icon: Icon(
                        Icons.photo_library_sharp,
                        size: 30,
                      )),
                    ]),
              ),
            ),
            Expanded(
                child: TabBarView(children: [
              _aboutTabBarView(context),
              Container(
                color: Colors.blue,
              ),
              Container(child: _galleryShowCase()),
            ]))
          ],
        ));
  }

  _galleryShowCase() {
    return Obx(
      () => profDetailPageCtrl.isGalleryShowcaseLoading.isTrue
          ? LoadingSpinner.showLoadingSpinner(
              context: context, text: "Fetching gallery list")
          : profDetailPageCtrl.galleryShowcaseData.isEmpty
              ? const Center(
                  child: Text("No data found !"),
                )
              : Padding(
                  padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                  child: GridView.custom(
                      gridDelegate: SliverQuiltedGridDelegate(
                          crossAxisCount: 3,
                          crossAxisSpacing: 5,
                          mainAxisSpacing: 5,
                          pattern: [
                            const QuiltedGridTile(1, 2),
                            const QuiltedGridTile(1, 1),
                            const QuiltedGridTile(1, 1),
                            const QuiltedGridTile(1, 2),
                          ]),
                      childrenDelegate: SliverChildBuilderDelegate(
                          childCount: profDetailPageCtrl
                              .galleryShowcaseData.length, (context, i) {
                        var data = profDetailPageCtrl.galleryShowcaseData[i];
                        return GestureDetector(
                          onTap: () {
                            Get.toNamed(MeroRoutesName.singlePhotoViewerPage,
                                arguments: "${data.image}".simpleMedia());
                          },
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(5.0),
                            child: FadedImageWidget(
                              imageUrl: "${data.image}".simpleMedia(),
                              imagePlaceHolder: AssetConst.bLogo,
                              imageError: AssetConst.bLogo,
                              fit: BoxFit.cover,
                            ),
                          ),
                        );
                      }))),
    );
  }

  _aboutTabBarView(BuildContext context) {
    return Expanded(
      child: Column(
        children: [
          _accountInfo(context,
              image: AssetConst.profession,
              title: "Profession",
              subtitle: profession
                  .toString()
                  .replaceAll("[", "")
                  .replaceAll("]", "")),
          _accountInfo(context,
              image: AssetConst.address,
              title: "Address",
              subtitle: address ?? ""),
        ],
      ),
    );
  }

  Widget _accountInfo(BuildContext context,
          {String? image, String? title, String? subtitle}) =>
      ListTile(
        leading: Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle, color: AppColors.appThemeColor),
          child: Padding(
            padding: const EdgeInsets.all(5.0),
            child: Image.asset(
              image ?? "",
              color: Colors.white,
              height: 0.040.h(context),
            ),
          ),
        ),
        title: Text(
          "$title",
          style: TextStyle(
              fontFamily: "Roboto",
              fontSize: 0.014.toResponsive(context),
              fontWeight: FontWeight.w500),
        ),
        subtitle: Text("$subtitle",
            style: TextStyle(
                fontFamily: "Roboto",
                fontSize: 0.012.toResponsive(context),
                fontWeight: FontWeight.w400)),
      );
}
