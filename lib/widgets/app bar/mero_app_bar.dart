import 'package:blue_works/constants/color.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MeroAppBar extends StatelessWidget implements PreferredSizeWidget {
  const MeroAppBar({
    Key? key,
    this.title,
    this.leading,
    this.widList,
    this.backgroundColor,
    this.statusBarColor = const Color(0xffF6F6F6),
    this.brightness = Brightness.dark,
    this.centerTile = false,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);
  final String? title;
  final Widget? leading;
  final List<Widget>? widList;
  final Color? backgroundColor;
  final Brightness? brightness;
  final Color? statusBarColor;
  final bool? centerTile;
  @override
  Widget build(BuildContext context) {
    return AppBar(
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light,
        statusBarColor: AppColors.appThemeColor,
      ),
      backgroundColor: backgroundColor,
      elevation: 0,
      automaticallyImplyLeading: false,
      title: Text(
        title ?? "",
        style: TextStyle(
            fontSize: 0.016.toResponsive(context),
            fontWeight: FontWeight.w500,
            fontFamily: "Roboto"),
      ),
      leading: leading,
      actions: widList,
      centerTitle: centerTile,
    );
  }

  @override
  final Size preferredSize;
}
