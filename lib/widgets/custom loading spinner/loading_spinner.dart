import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class LoadingSpinner {
  LoadingSpinner._();
  factory LoadingSpinner() => instance;
  static LoadingSpinner instance = LoadingSpinner._();

  static showLoadingSpinner({required BuildContext context, String? text}) {
    return Padding(
      padding: EdgeInsets.only(top: 0.02.h(context)),
      child: Column(
        children: [
          const CircularProgressIndicator(),
          SizedBox(height: 0.015.h(context)),
          Shimmer.fromColors(
              baseColor: Colors.grey,
              highlightColor: Colors.white,
              child: Text(
                text ?? "Fetching data",
                style: TextStyle(fontSize: 0.014.toResponsive(context)),
              ))
        ],
      ),
    );
  }
}
