// import 'package:flutter/material.dart';
// import 'dart:math' as math;

// class CustomLoadingSpinnerPainter extends CustomPainter {
//   final double _currentAnimationValue;
//   final Color _color;

//   CustomLoadingSpinnerPainter(this._currentAnimationValue, this._color);

//   @override
//   void paint(Canvas canvas, Size size) {
//     const double strokeWidth = 12;
//     final double halfWidth = size.width / 2;
//     final double halfHeight = size.height / 2;
//     final double radius = math.min(halfWidth, halfHeight) - strokeWidth / 2;

//     final Paint paint = Paint()
//       ..color = _color
//       ..strokeWidth = strokeWidth
//       ..style = PaintingStyle.stroke
//       ..strokeCap = StrokeCap.round;

//     final double progressAngle = _currentAnimationValue * 3.5 * math.pi;
//     final double endAngle = progressAngle + 0.8 * math.pi;

//     canvas.drawArc(
//       Rect.fromCircle(center: Offset(halfWidth, halfHeight), radius: radius),
//       progressAngle,
//       endAngle - progressAngle,
//       false,
//       paint,
//     );

//     final tailPaint = Paint()
//       ..color = _color
//       ..strokeWidth = strokeWidth
//       ..style = PaintingStyle.stroke
//       ..strokeCap = StrokeCap.round;

//     final double tailLength = radius / 4;
//     final double tailAngle = endAngle * math.pi;
//     final double tailX = halfWidth + radius * math.cos(tailAngle);
//     final double tailY = halfHeight + radius * math.sin(tailAngle);

//     final tailStart = Offset(tailX, tailY);
//     final tailEnd = Offset(
//       tailX + tailLength * math.cos(tailAngle + 0.5 * math.pi),
//       tailY + tailLength * math.sin(tailAngle + 0.5 * math.pi),
//     );

//     canvas.drawLine(tailStart, tailEnd, tailPaint);
//   }

//   @override
//   bool shouldRepaint(CustomLoadingSpinnerPainter oldDelegate) =>
//       _currentAnimationValue != oldDelegate._currentAnimationValue;
// }

// class LoadingSpinner extends StatefulWidget {
//   final Color color;

//   const LoadingSpinner({super.key, this.color = Colors.blue});

//   @override
//   State<LoadingSpinner> createState() => _LoadingSpinnerState();
// }

// class _LoadingSpinnerState extends State<LoadingSpinner>
//     with SingleTickerProviderStateMixin {
//   late AnimationController _animationController;

//   @override
//   void initState() {
//     _animationController = AnimationController(
//       vsync: this,
//       duration: const Duration(milliseconds: 1000),
//     )..repeat();
//     super.initState();
//   }

//   @override
//   void dispose() {
//     _animationController.dispose();
//     super.dispose();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return AnimatedBuilder(
//       animation: _animationController,
//       builder: (context, child) {
//         return CustomPaint(
//           painter: CustomLoadingSpinnerPainter(
//               _animationController.value, widget.color),
//           size: const Size.square(80),
//         );
//       },
//     );
//   }
// }
