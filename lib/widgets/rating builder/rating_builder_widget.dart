import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RatingBuilderWidget extends StatelessWidget {
  const RatingBuilderWidget(
      {super.key, this.initialRating, this.ignoreGesture = true});
  final double? initialRating;
  final bool ignoreGesture;
  @override
  Widget build(BuildContext context) {
    return RatingBar.builder(
      initialRating: initialRating ?? 0.0,
      minRating: 0,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemPadding: const EdgeInsets.symmetric(horizontal: 0.0),
      itemBuilder: (context, _) => const Icon(
        Icons.star,
        color: Colors.amber,
      ),
      ignoreGestures: ignoreGesture,
      onRatingUpdate: (rating) {},
    );
  }
}
