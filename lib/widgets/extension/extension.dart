import 'package:flutter/material.dart';

import '../../constants/api_urls.dart';

getSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

extension GetSize on double {
  double toResponsive(BuildContext context) {
    return (getSize(context).height * this) + (getSize(context).width * this);
  }

  double h(BuildContext context) {
    return MediaQuery.of(context).size.height * this;
  }

  double w(BuildContext context) {
    return MediaQuery.of(context).size.width * this;
  }
}

extension Media on String {
  String simpleMedia() {
    String value = this;
    bool isUrl = Uri.tryParse(value)!.host.isNotEmpty;
    if (isUrl || value.contains("http") || value.contains("https")) {
      return this;
    } else {
      return "${ApiUrls.mediaUrl}$this";
    }
  }
}
