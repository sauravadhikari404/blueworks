import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class RichTextStyle {
  RichTextStyle._();
  factory RichTextStyle() => instance;
  static RichTextStyle instance = RichTextStyle._();

  static richTextWidget(
      {String? text1,
      String? text2,
      required BuildContext context,
      Color? color1,
      Color? color,
      double? text1FontSize,
      double? text2FontSize,
      TextDecoration? txtDecoration,
      String? fontFamily,
      VoidCallback? tapFunction}) {
    return Text.rich(
      TextSpan(
        text: '$text1 ',
        style: TextStyle(
          fontSize: text1FontSize ?? 0.012.toResponsive(context),
          color: color1,
          fontFamily: fontFamily,
        ),
        children: <TextSpan>[
          TextSpan(
              recognizer: TapGestureRecognizer()..onTap = tapFunction,
              text: "$text2",
              style: TextStyle(
                  fontSize: text2FontSize ?? 0.012.toResponsive(context),
                  color: color,
                  decoration: txtDecoration,
                  fontFamily: fontFamily)),
        ],
      ),
    );
  }
}
