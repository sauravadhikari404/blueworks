import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';

class TextFormFieldWidget extends StatelessWidget {
  const TextFormFieldWidget(
      {super.key,
      this.title,
      this.inputType,
      this.controller,
      this.filled = true,
      this.hintText,
      this.prefixIcon,
      this.fillColor = Colors.white,
      this.obsecure = false,
      this.suffixIcon,
      this.prefixWid,
      this.txtCapital});
  final String? title;
  final String? hintText;
  final TextInputType? inputType;
  final TextEditingController? controller;
  final bool filled;
  final Widget? prefixIcon;
  final Color? fillColor;
  final bool obsecure;
  final Widget? suffixIcon;
  final Widget? prefixWid;
  final TextCapitalization? txtCapital;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
          errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 1),
              borderRadius: BorderRadius.circular(5)),
          errorStyle: TextStyle(fontSize: 0.0.toResponsive(context)),
          fillColor: fillColor,
          filled: filled,
          prefixIcon: prefixIcon,
          hintText: '$title',
          suffixIcon: suffixIcon,
          prefix: prefixWid,
          hintStyle: TextStyle(
              fontSize: 0.012.toResponsive(context),
              color: Colors.black26,
              fontFamily: "Roboto")),
      keyboardType: inputType,
      controller: controller,
      obscureText: obsecure,
      validator: (value) {
        if (value == null) {
          return "";
        } else if (value.isEmpty) {
          return "";
        } else {
          return null;
        }
      },
      textCapitalization: txtCapital ?? TextCapitalization.none,
    );
  }
}
