import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/enum.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';

class SearchTextFormField extends StatelessWidget {
  const SearchTextFormField(
      {super.key,
      this.title,
      this.inputType,
      this.controller,
      this.filled = true,
      this.hintText,
      this.prefixIcon,
      this.fillColor = Colors.white,
      this.obsecure = false,
      this.suffixIcon,
      this.prefixWid,
      this.txtCapital,
      this.pk,
      this.page,
      this.whichPage});
  final String? title;
  final String? hintText;
  final TextInputType? inputType;
  final TextEditingController? controller;
  final bool filled;
  final Widget? prefixIcon;
  final Color? fillColor;
  final bool obsecure;
  final Widget? suffixIcon;
  final Widget? prefixWid;
  final TextCapitalization? txtCapital;
  final int? pk;
  final int? page;
  final WHICHPAGE? whichPage;
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(14.0),
          border: const OutlineInputBorder(
            borderSide: BorderSide.none,
          ),
          errorBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.red, width: 1),
              borderRadius: BorderRadius.circular(5)),
          enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black38, width: 1),
              borderRadius: BorderRadius.circular(10)),
          focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black38, width: 1),
              borderRadius: BorderRadius.circular(10)),
          errorStyle: TextStyle(fontSize: 0.0.toResponsive(context)),
          fillColor: fillColor,
          filled: filled,
          prefixIcon: prefixIcon,
          hintText: '$title',
          suffixIcon: suffixIcon,
          prefix: prefixWid,
          hintStyle: TextStyle(
              fontSize: 0.012.toResponsive(context),
              color: Colors.black26,
              fontFamily: "Roboto")),
      keyboardType: inputType,
      controller: controller,
      obscureText: obsecure,
      onChanged: (value) {
        globalCtrl.startTimer(context: context);
        switch (whichPage ?? "") {
          case WHICHPAGE.professionalPage:
            profCtrl.fetchProfessionals(
                pk: pk ?? 0, searchText: value, page: page ?? 1);
            break;
          case WHICHPAGE.topProfessionalPage:
            profCatCtrl.fetchTopProfessionals(
                page: page ?? 1, searchTxt: value);
        }
      },
      textCapitalization: txtCapital ?? TextCapitalization.none,
    );
  }
}
