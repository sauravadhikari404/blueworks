import 'package:blue_works/controller/theme_controller.dart';
import 'package:blue_works/local%20storage/local_storage.dart';
import 'package:blue_works/my_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    //DeviceOrientation.portraitDown,
    // DeviceOrientation.landscapeLeft,
    // DeviceOrientation.landscapeRight
  ]);
  await LocalStorage().init();
  Get.put(ThemeController(), permanent: true);
  Get.lazyPut(() => ThemeController());
  runApp(const MyApp());
}
