import 'package:blue_works/models/professional_category_model.dart';
import 'package:blue_works/services/services.dart';
import 'package:get/get.dart';

import '../models/slide_show_model.dart';
import '../screens/top professionals screen/model/top_professionals_model.dart';

class ProfessionalCategoryController extends GetxController {
  static ProfessionalCategoryController instance = Get.find();
  RxList<ProfessionalCategoryModel> professionalCategoryList = RxList();
  Rx<TopProfessionalsModel?> topProfessionalData = Rx(TopProfessionalsModel());
  RxList<SlideShowModel> slideShowList = RxList();
  RxBool isLoading = true.obs;
  RxBool isLoading1 = true.obs;
  RxBool isLoading2 = true.obs;
  RxBool isLoadingMoreData = false.obs;
  int topProfPage = 1;
  RxBool toggleNearBy = false.obs;

  ///This function is used to fetch professional category like plumber, painter
  Future<void> fetchProfessionalCategoryModel() async {
    try {
      isLoading(true);
      professionalCategoryList.value =
          (await Services.fetchProfessionalCategories()) ??
              <ProfessionalCategoryModel>[];
    } finally {
      isLoading(false);
    }
  }

  ///This function is used to fetch top professionals list
  Future<void> fetchTopProfessionals(
      {required int page,
      bool isRefresh = false,
      String searchTxt = "",
      double? latitude,
      double? longitude}) async {
    try {
      isRefresh ? isLoadingMoreData(true) : isLoading2(true);
      var data = await Services.fetchTopProfessionals(
          page: page,
          searchText: searchTxt,
          latitude: latitude,
          longitude: longitude);
      isRefresh
          ? topProfessionalData.value?.results?.addAll(data?.results ?? [])
          : topProfessionalData.value = data;
    } finally {
      isLoading2(false);
    }
  }

  ///This function is used to fetch slider which is shown in homepage
  Future<void> fetchSlider() async {
    try {
      isLoading1(true);
      slideShowList.value =
          (await Services.fetchSlideShows()) ?? <SlideShowModel>[];
    } finally {
      isLoading1(false);
    }
  }

  @override
  void onInit() {
    fetchSlider();
    super.onInit();
  }

  @override
  void onReady() {
    fetchSlider();
    super.onReady();
  }
}
