import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart' as path;

import '../services/services.dart';
import '../widgets/dialogue boxes/dialogue_boxes.dart';

class GlobalController extends GetxController {
  static GlobalController instance = Get.find();
  RxBool isLoading = true.obs;
  RxList<File> compressedFiles = <File>[].obs;
  Rx<File>? singleCompressedFile = Rx(File(""));
  Timer? timer;

  ///This function is used to logout user
  Future<void> logoutUser() async {
    try {
      DialogueBoxes.showLoadingDialogue();
      await Services.logoutUser();
    } finally {
      DialogueBoxes.dismissLoadingDialog();
    }
  }

  ///Unfocus the keyboard when tapped on body of the screen
  unFocusKeybaord(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  ///This function is just an simple name concaniter like concatanating first name middle name and last name
  String concatName(
      {required String firstName,
      String? middleName,
      required String lastName}) {
    if (middleName == null || middleName.isEmpty) {
      return "$firstName $lastName";
    } else {
      return "$firstName $middleName $lastName";
    }
  }

  ///This function is just an simple address concaniter like city, address, street concatination
  String concatAddress({String? city, String? country, String? street}) {
    return "$country-$city-$street";
  }

  ///This function is used to get device orientation
  Orientation devOrient(BuildContext context) {
    return MediaQuery.of(context).orientation;
  }

  String getCountryISOCode() {
    final WidgetsBinding instance = WidgetsBinding.instance;
    final List<Locale> systemLocales = instance.window.locales;
    String? isoCountryCode = systemLocales.first.countryCode;
    if (isoCountryCode != null) {
      log(isoCountryCode);
      return isoCountryCode;
    } else {
      throw Exception("Unable to get Country ISO code");
    }
  }

  ///This function is used to pick multiple attachments from mobile phone like images
  void pickImages() async {
    List<File> oldFiles = compressedFiles.isEmpty ? [] : compressedFiles;
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          allowMultiple: true,
          allowCompression: true,
          type: FileType.custom,
          dialogTitle: "Pick Photos",
          allowedExtensions: [
            'jpg',
            'jpeg',
            'png',
          ]);
      if (result != null) {
        List<File> data =
            result.paths.map((path) => File(path ?? "")).toList(growable: true);
        data.addAll(oldFiles);
        for (int i = 0; i < data.length; i++) {
          if (checkFileTypeUrl(data[i].path) == "jpg") {
            File compressedFile = await attachmentCompressor(
                data[i], data[i].path.split('/').last);
            compressedFiles.add(compressedFile);
            debugPrint("compressed and add");
          } else {
            compressedFiles.add(data[i]);
          }
        }
      } else {
        debugPrint("No files selected !");
      }
    } catch (e) {
      debugPrint("Error occurred ${e.toString()}");
    }
  }

  ///This is used to pick single image from mobile phone
  pickSingleImage() async {
    try {
      FilePickerResult? result = await FilePicker.platform.pickFiles(
          allowCompression: true,
          type: FileType.custom,
          dialogTitle: "Pick Photos",
          allowedExtensions: [
            'jpg',
            'jpeg',
            'png',
          ]);
      if (result != null) {
        File file = File(result.files.single.path ?? "");
        log("This is file $file");
        singleCompressedFile?.value = file;
        //singleCompressedFile?.value = file;

        log("This is compressedFile $singleCompressedFile");
        // if (checkFileTypeUrl(file.path) == "jpg") {
        //   File compressedFile =
        //       await attachmentCompressor(file, file.path.split('/').last);
        //   log("This is compressedFile $compressedFile");
        //   singleCompressedFile?.value = compressedFile;
        //   return compressedFile.path;
        // }
      } else {
        debugPrint("No files selected !");
        return "";
      }
    } catch (e) {
      debugPrint("Error occurred ${e.toString()}");
    }
  }

  ///This is used to compress image
  Future<File> attachmentCompressor(File mFiles, String pickedFileName) async {
    try {
      final dir = await path.getTemporaryDirectory();
      final targetPath = '${dir.absolute.path}/$pickedFileName';
      var result = await FlutterImageCompress.compressAndGetFile(
          mFiles.path, targetPath,
          quality: 90, minWidth: 1280, minHeight: 720);
      return File(result!.path);
    } catch (e) {
      debugPrint("error occurred ${e.toString()}");
      rethrow;
    }
  }

  ///This is used to check the type of file like if it is .pdf or .jpg or .jpeg or .mov or .mp4.........
  checkFileTypeUrl(String? file) {
    if (file != null) {
      String extension = file.split('?')[0].split(".").last;
      debugPrint("This is extension $extension");
      if (extension == "pdf") {
        return "pdf";
      } else if (extension == "doc" || extension == "docx") {
        return "doc";
      } else if (extension == "ppt" || extension == "pptx") {
        return "ppt";
      } else if (extension == "xls" || extension == "xlsx") {
        return "xls";
      } else if (extension == "txt") {
        return "txt";
      } else if (extension == "jpg" ||
          extension == "jpeg" ||
          extension == "png" ||
          extension == "gif" ||
          extension == "webp") {
        return "jpg";
      } else if (extension == "mp4") {
        return "mp4";
      } else if (extension == "mp3") {
        return "mp3";
      } else if (extension == "zip") {
        return "zip";
      } else if (extension == "rar") {
        return "rar";
      } else if (extension == "mov" ||
          extension == "flv" ||
          extension == "mkv" ||
          extension == "wmv" ||
          extension == "avi" ||
          extension == "webm" ||
          extension == "mpeg-2" ||
          extension == "avchd" ||
          extension == "f4v" ||
          extension == "swf") {
        return "mov";
      } else {
        return "other";
      }
    } else {
      return "other";
    }
  }

  ///Used to set the statusbar color of specific page
  setStatusBarColor({required Color colorss}) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: colorss, // set the status bar color here
    ));
    update();
  }

  ///When we set status bar color in init method we need to dispose it so other page should no be harmed so this method is used to dispose it
  removeStatusBarColor() {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      statusBarColor: Colors.transparent, // set the status bar color here
    ));
    update();
  }

  //Fetch user current latitude and longitude
  Future<Position> determinePosition() async {
    LocationPermission permission;

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }

    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
        timeLimit: const Duration(seconds: 30));
  }

  ///This is used to close keyboard automatically when user stops typing in textform field mostly used in search text form field
  void startTimer({required BuildContext context}) {
    if (timer != null) {
      timer!.cancel();
    }

    timer = Timer(const Duration(seconds: 5), () {
      unFocusKeybaord(context);
    });
  }
}
