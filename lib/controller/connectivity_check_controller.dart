import 'dart:async';
import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/keys.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class GetXNetworkManager extends GetxController {
  static GetXNetworkManager instance = Get.find();

  ///this variable 0 = No Internet, 1 = connected to WIFI ,2 = connected to Mobile Data.
  RxInt connectionType = 0.obs;

  ///Instance of Flutter Connectivity
  final Connectivity _connectivity = Connectivity();

  ///Stream to keep listening to network change state
  late StreamSubscription _streamSubscription;

  /// a method to get which connection result, if you we connected to internet or no if yes then which network
  Future<void> getConnectionType() async {
    ConnectivityResult? connectivityResult;
    try {
      connectivityResult = await (_connectivity.checkConnectivity());
    } on PlatformException catch (e) {
      logger.d("Platform exception", e.toString());
    }
    return _updateState(connectivityResult);
  }

  ///state update, of network, if you are connected to WIFI connectionType will get set to 1,
  /// update the state to the consumer of that variable.
  _updateState(ConnectivityResult? result) {
    switch (result) {
      case ConnectivityResult.wifi:
        connectionType.value = 1;
        update();
        break;
      case ConnectivityResult.mobile:
        DialogueBoxes.flutterToastDialogueBox("Connected to mobile data",
            color: AppColors.successColor);
        connectionType.value = 2;
        update();
        break;
      case ConnectivityResult.none:
        DialogueBoxes.flutterToastDialogueBox("No Internet",
            color: AppColors.errorColor);
        connectionType.value = 0;
        update();
        break;
      default:
        DialogueBoxes.flutterToastDialogueBox("Failed to get Network Status",
            color: AppColors.warningColor);
        break;
    }
  }

  @override
  void onInit() {
    getConnectionType();
    _streamSubscription =
        _connectivity.onConnectivityChanged.listen(_updateState);
    super.onInit();
  }

  @override
  void onClose() {
    ///stop listening to network state when app is closed
    _streamSubscription.cancel();
  }
}
