import 'package:blue_works/services/services.dart';
import 'package:get/get.dart';

import '../models/gallery_show_case_model.dart';

class ProfessionalDetailPageController extends GetxController {
  static ProfessionalDetailPageController instance = Get.find();
  RxBool isGalleryShowcaseLoading = true.obs;
  RxList<GalleryShowCaseModel> galleryShowcaseData = RxList();

  ///used to fetch gallery image of the professional
  Future<void> fetchGalleryShowCaseList({int? id}) async {
    try {
      isGalleryShowcaseLoading(true);
      galleryShowcaseData.value = await Services.fetchGalleryShowCase(id: id);
    } finally {
      isGalleryShowcaseLoading(false);
    }
  }
}
