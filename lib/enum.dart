///Model Handler enum

enum MH {
  ///Post method enums
  loginModel,
  register,
  changePassword,
  logoutUser,

  ///Get method enums
  professionalCategoryModel,
  slideShowModel,
  userProfileModel,
  topProfessionalsModel,
  professionalsModel,
  galleryShowCaseModel,
}

enum WHICHPAGE { professionalPage, topProfessionalPage }
