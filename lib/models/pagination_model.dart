class Pagination {
  Pagination({
    this.itemsPerPage,
    this.totalItems,
    this.totalPages,
    this.currentPageNumber,
    this.next,
    this.previous,
  });

  int? itemsPerPage;
  int? totalItems;
  int? totalPages;
  int? currentPageNumber;
  String? next;
  String? previous;

  factory Pagination.fromJson(Map<String, dynamic> json) => Pagination(
        itemsPerPage: json["items_per_page"],
        totalItems: json["total_items"],
        totalPages: json["total_pages"],
        currentPageNumber: json["current_page_number"],
        next: json["next"],
        previous: json["previous"],
      );

  Map<String, dynamic> toJson() => {
        "items_per_page": itemsPerPage,
        "total_items": totalItems,
        "total_pages": totalPages,
        "current_page_number": currentPageNumber,
        "next": next,
        "previous": previous,
      };
}
