import 'dart:convert';

List<GalleryShowCaseModel> galleryShowCaseModelFromJson(String str) =>
    List<GalleryShowCaseModel>.from(
        json.decode(str).map((x) => GalleryShowCaseModel.fromJson(x)));

String galleryShowCaseModelToJson(List<GalleryShowCaseModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class GalleryShowCaseModel {
  int? id;
  bool? isFeatured;
  String? image;

  GalleryShowCaseModel({
    this.id,
    this.isFeatured,
    this.image,
  });

  factory GalleryShowCaseModel.fromJson(Map<String, dynamic> json) =>
      GalleryShowCaseModel(
        id: json["id"],
        isFeatured: json["isFeatured"],
        image: json["image"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "isFeatured": isFeatured,
        "image": image,
      };
}
