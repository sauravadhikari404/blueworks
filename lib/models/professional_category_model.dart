import 'dart:convert';

List<ProfessionalCategoryModel> professionalCategoryModelFromJson(String str) =>
    List<ProfessionalCategoryModel>.from(
        json.decode(str).map((x) => ProfessionalCategoryModel.fromJson(x)));

String professionalCategoryModelToJson(List<ProfessionalCategoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class ProfessionalCategoryModel {
  ProfessionalCategoryModel(
      {this.id, this.icon, this.name, this.description, this.categoryId});

  int? id;
  String? icon;
  String? name;
  String? description;
  int? categoryId;

  factory ProfessionalCategoryModel.fromJson(Map<String, dynamic> json) =>
      ProfessionalCategoryModel(
          id: json["id"],
          icon: json["icon"],
          name: json["name"],
          description: json["description"],
          categoryId: json["category_id"]);

  Map<String, dynamic> toJson() => {
        "id": id,
        "icon": icon,
        "name": name,
        "description": description,
        "category_id": categoryId
      };
}
