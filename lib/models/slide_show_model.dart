import 'dart:convert';

List<SlideShowModel> slideShowModelFromJson(String str) =>
    List<SlideShowModel>.from(
        json.decode(str).map((x) => SlideShowModel.fromJson(x)));

String slideShowModelToJson(List<SlideShowModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class SlideShowModel {
  SlideShowModel({
    this.id,
    this.image,
    this.title,
    this.description,
    this.url,
    this.isActive,
  });

  int? id;
  String? image;
  String? title;
  String? description;
  String? url;
  bool? isActive;

  factory SlideShowModel.fromJson(Map<String, dynamic> json) => SlideShowModel(
        id: json["id"],
        image: json["image"],
        title: json["title"],
        description: json["description"],
        url: json["url"],
        isActive: json["is_active"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "image": image,
        "title": title,
        "description": description,
        "url": url,
        "is_active": isActive,
      };
}
