import 'dart:convert';

UserProfileModel userProfileModelFromJson(String str) =>
    UserProfileModel.fromJson(json.decode(str));

String userProfileModelToJson(UserProfileModel data) =>
    json.encode(data.toJson());

class UserProfileModel {
  UserProfileModel({
    this.user,
    this.dob,
    this.address,
    this.profilePic,
    this.profession,
    //this.rating,
    this.socialNetworks,
    this.grids,
  });

  User? user;
  dynamic dob;
  Address? address;
  String? profilePic;
  List<dynamic>? profession;
  //List<dynamic>? rating;
  List<dynamic>? socialNetworks;
  int? grids;

  factory UserProfileModel.fromJson(Map<String, dynamic> json) =>
      UserProfileModel(
        user: User.fromJson(json["user"]),
        dob: json["dob"],
        address: Address.fromJson(json["address"]),
        profilePic: json["profile_pic"] ?? "",
        profession: List<dynamic>.from(json["profession"].map((x) => x)),
        //rating: List<dynamic>.from(json["rating"].map((x) => x)),
        socialNetworks:
            List<dynamic>.from(json["social_networks"].map((x) => x)),
        grids: json["grids"],
      );

  Map<String, dynamic> toJson() => {
        "user": user?.toJson(),
        "dob": dob,
        "address": address?.toJson(),
        "profile_pic": profilePic,
        "profession": List<dynamic>.from(profession?.map((x) => x) ?? []),
        //"rating": List<dynamic>.from(rating?.map((x) => x) ?? []),
        "social_networks":
            List<dynamic>.from(socialNetworks?.map((x) => x) ?? []),
        "grids": grids,
      };
}

class Address {
  Address({
    this.street,
    this.city,
    this.country,
    this.latitude,
    this.longitude,
  });

  String? street;
  String? city;
  String? country;
  double? latitude;
  double? longitude;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        street: json["street"],
        city: json["city"],
        country: json["country"],
        latitude: json["latitude"] ?? "",
        longitude: json["longitude"] ?? "",
      );

  Map<String, dynamic> toJson() => {
        "street": street,
        "city": city,
        "country": country,
        "latitude": latitude,
        "longitude": longitude,
      };
}

class User {
  User({
    required this.id,
    required this.phone,
    this.email,
    required this.firstName,
    required this.middleName,
    required this.lastName,
    required this.role,
  });

  int id;
  String phone;
  dynamic email;
  String firstName;
  String middleName;
  String lastName;
  String role;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        phone: json["phone"],
        email: json["email"],
        firstName: json["first_name"],
        middleName: json["middle_name"] ?? "",
        lastName: json["last_name"],
        role: json["role"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "phone": phone,
        "email": email,
        "first_name": firstName,
        "middle_name": middleName,
        "last_name": lastName,
        "role": role,
      };
}
