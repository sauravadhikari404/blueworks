import 'package:blue_works/config/theme/ui_parameters.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../../constants/color.dart';

enum FaButtonType { extended, small, larger }

mixin SubThemeData {
  AppBarTheme getAppBarThemeLight() {
    return AppBarTheme(
        elevation: 0,
        backgroundColor: AppColors.appThemeColor,
        titleTextStyle: const TextStyle(
            fontWeight: FontWeight.w600, fontFamily: 'Roboto', fontSize: 20));
  }

  AppBarTheme getAppBarThemeDark() {
    return const AppBarTheme(
        elevation: 0,
        backgroundColor: AppColors.kPrimaryColorDark,
        titleTextStyle: TextStyle(
            fontWeight: FontWeight.w600, fontFamily: 'Roboto', fontSize: 20));
  }

  FloatingActionButtonThemeData getFloatingActionButtonThemeLight() {
    return FloatingActionButtonThemeData(
      backgroundColor: AppColors.appThemeColor,
      extendedTextStyle: const TextStyle(
        fontFamily: 'Roboto',
      ),
      splashColor: AppColors.appThemeColor,
    );
  }

  FloatingActionButtonThemeData getFloatingActionButtonThemeDark() {
    return const FloatingActionButtonThemeData(
      backgroundColor: AppColors.kPrimaryColorDark,
      extendedTextStyle: TextStyle(
        fontFamily: 'Roboto',
      ),
      splashColor: AppColors.kPrimaryColorDarkDim,
    );
  }

  static List<BoxShadow> getGridItemBoxShadow(BuildContext context) =>
      UIParameters.isDarkMode(context)
          ? [
              const BoxShadow(
                  color: Colors.teal, blurRadius: 3, offset: Offset(1, 1)),
              BoxShadow(
                  color: Theme.of(context).primaryColor,
                  blurRadius: 5,
                  offset: const Offset(1, 2)),
            ]
          : const [
              BoxShadow(
                  color: Colors.teal, blurRadius: 3, offset: Offset(1, 1)),
              BoxShadow(
                  color: Colors.white, blurRadius: 5, offset: Offset(1, 2)),
              BoxShadow(color: Colors.white, blurRadius: 8),
            ];

  static List<BoxShadow> getListItemBoxShadow(BuildContext context) =>
      UIParameters.isDarkMode(context)
          ? [
              const BoxShadow(
                  color: Colors.teal, blurRadius: 3, offset: Offset(1, 1)),
              BoxShadow(
                  color: Theme.of(context).primaryColor,
                  blurRadius: 5,
                  offset: const Offset(1, 2)),
            ]
          : [
              BoxShadow(
                  color: AppColors.appThemeColor,
                  blurRadius: 0.5,
                  spreadRadius: 0.5),
              const BoxShadow(
                  color: Colors.white, blurRadius: 1.0, spreadRadius: 1.5),
              const BoxShadow(
                  color: Colors.black87, blurRadius: 1.0, spreadRadius: 1.5),
              const BoxShadow(
                  color: Colors.white, blurRadius: 1.5, spreadRadius: 1.8),
            ];

  // BottomAppBarTheme getBottomAppBarTheme() {
  //   return const BottomAppBarTheme(color: Colors.transparent, elevation: 0);
  // }

  TextTheme getTextThemes() {
    return GoogleFonts.quicksandTextTheme(const TextTheme(
        bodyLarge: TextStyle(fontWeight: FontWeight.w400),
        bodyMedium: TextStyle(fontWeight: FontWeight.w400)));
  }

  IconThemeData getPrimaryIconThemeData() {
    return const IconThemeData(
      color: AppColors.kIconColorLT,
      size: 20,
    );
  }

  TextButtonThemeData getTextButtonThemeData() {
    return TextButtonThemeData(style: getElavatedButtonTheme());
  }

  // InputDecorationTheme getInputDecoration() {
  //   return const InputDecorationTheme();
  // }

  IconThemeData getIconTheme() {
    return IconThemeData(
      color: AppColors.appThemeColor,
      size: 18,
    );
  }

  ButtonStyle getElavatedButtonTheme() {
    return ElevatedButton.styleFrom(
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
    );
  }

  BottomNavigationBarThemeData getBottomNavigationBarTheme() {
    return const BottomNavigationBarThemeData(
        type: BottomNavigationBarType.fixed,
        elevation: 10,
        showSelectedLabels: false,
        showUnselectedLabels: false);
  }
}
