import 'package:blue_works/config/theme/sub_theme_mixin.dart';
import 'package:blue_works/constants/color.dart';
import 'package:flutter/material.dart';

class DarkTheme with SubThemeData {
  ThemeData buildDarkTheme() {
    final ThemeData systemDarkTheme = ThemeData.dark();
    return systemDarkTheme.copyWith(
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: AppColors.kScafoldBacgroundColorDark,
        splashColor: AppColors.kPrimaryColorDark.withOpacity(0.1),
        cardTheme: const CardTheme(
          clipBehavior: Clip.antiAlias,
          color: AppColors.kCardColorDK,
        ),
        iconTheme: const IconThemeData(
          color: AppColors.kPrimaryTextLightColor,
        ),
        //floating action btn theme
        floatingActionButtonTheme: getFloatingActionButtonThemeDark(),
        //appbar theme
        appBarTheme: getAppBarThemeDark(),
        highlightColor: AppColors.appThemeColor,
        textTheme: getTextThemes().apply(
            bodyColor: AppColors.kPrimaryTextLightColor,
            displayColor: AppColors.kPrimaryTextLightColor),
        cardColor: AppColors.kCardColorDK,
        primaryColor: AppColors.kPrimaryColorDark);
  }
}
