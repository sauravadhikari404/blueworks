import 'package:blue_works/config/theme/sub_theme_mixin.dart';
import 'package:flutter/material.dart';

import '../../constants/color.dart';

class LightTheme with SubThemeData {
  ThemeData buildLightTheme() {
    final ThemeData systemLightTheme = ThemeData.light();
    return systemLightTheme.copyWith(
        visualDensity: VisualDensity.adaptivePlatformDensity,

        ///This is our body color
        scaffoldBackgroundColor: AppColors.bodyColor,
        iconTheme: getIconTheme(),
        appBarTheme: getAppBarThemeLight(),
        floatingActionButtonTheme: getFloatingActionButtonThemeLight(),
        //splashColor: AppColors.appThemeColor,
        //highlightColor: AppColors.appThemeColor,
        splashFactory: InkRipple.splashFactory,
        textTheme: getTextThemes().apply(
            bodyColor: AppColors.textColor,
            displayColor: AppColors.kPrimaryTextColorLT),
        primaryColor: AppColors.appThemeColor,
        cardColor: AppColors.kCardColorLT,
        colorScheme: ColorScheme.fromSwatch(
            accentColor: AppColors.appThemeColor, primarySwatch: Colors.blue),
        buttonTheme: ButtonThemeData(
          splashColor: AppColors.appThemeColor,
          buttonColor: Colors.white,
        ),
        canvasColor: Colors.black,
        drawerTheme: const DrawerThemeData(
            backgroundColor: AppColors.kScafoldBacgroundColor, elevation: 2),

        //list tile theme
        listTileTheme: ListTileThemeData(
            textColor: AppColors.kPrimaryTextColor,
            iconColor: AppColors.appThemeColor));
  }
}
