import 'package:blue_works/controller/global_controller.dart';
import 'package:blue_works/screens/auth%20screens/controller/auth_controller.dart';
import 'package:get/get.dart';

import '../controller/connectivity_check_controller.dart';

class Binder extends Bindings {
  @override
  void dependencies() {
    Get.put(GlobalController(), permanent: true);
    Get.lazyPut(() => GlobalController());
    Get.put(AuthController(), permanent: true);
    Get.put(GetXNetworkManager());
  }
}
