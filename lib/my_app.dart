import 'package:blue_works/Binder/binder.dart';
import 'package:blue_works/route/route.dart';
import 'package:blue_works/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/theme_controller.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'BlueMate',
      debugShowCheckedModeBanner: false,
      locale: Get.deviceLocale,
      opaqueRoute: true,
      initialBinding: Binder(),
      smartManagement: SmartManagement.full,
      defaultTransition: Transition.cupertinoDialog,
      themeMode: Get.find<ThemeController>().getThemeMode(),
      darkTheme: Get.find<ThemeController>().getDarkTheme(),
      theme: Get.find<ThemeController>().getLightheme(),
      getPages: MeroRoutes.routes,
      home: const SplashScreen(),
    );
  }
}
