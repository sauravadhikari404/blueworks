import 'dart:convert';

import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/enum.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/services/model_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../widgets/dialogue boxes/dialogue_boxes.dart';

class ResponseHandler {
  ResponseHandler._();
  factory ResponseHandler() => instance;
  static ResponseHandler instance = ResponseHandler._();

  static getResponse({required http.Response response, MH? modelManager}) {
    switch (response.statusCode) {
      case 200:
        return ModelHandler.addDataToModel(
            modelManager: modelManager, response: response);
      case 401:
        globalCtrl.logoutUser();
        DialogueBoxes.flutterToastDialogueBox("Session Expired",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
      case 404:
        DialogueBoxes.flutterToastDialogueBox("Data not found",
            color: AppColors.warningColor, length: Toast.LENGTH_LONG);
        break;
      case 500:
        DialogueBoxes.flutterToastDialogueBox("Server Error",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
      case 501:
        DialogueBoxes.flutterToastDialogueBox("Server Error",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
    }
  }

  static postResponse(http.Response response, {MH? modelManager}) {
    var decodeResponse = jsonDecode(response.body);
    switch (response.statusCode) {
      case 200:
        DialogueBoxes.dismissLoadingDialog();
        return ModelHandler.addDataToModel(
            modelManager: modelManager,
            response: response,
            decodedResponse: decodeResponse);
      case 201:
        DialogueBoxes.dismissLoadingDialog();
        return ModelHandler.addDataToModel(
            modelManager: modelManager,
            response: response,
            decodedResponse: decodeResponse);
      case 401:
        DialogueBoxes.dismissLoadingDialog();
        Get.offAllNamed(MeroRoutesName.loginPage);
        DialogueBoxes.flutterToastDialogueBox("${decodeResponse["message"]}",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
      case 404:
        DialogueBoxes.dismissLoadingDialog();
        DialogueBoxes.flutterToastDialogueBox("${decodeResponse["message"]}",
            color: AppColors.warningColor, length: Toast.LENGTH_LONG);
        break;
      case 400:
        DialogueBoxes.dismissLoadingDialog();
        DialogueBoxes.flutterToastDialogueBox("${decodeResponse["message"]}",
            color: AppColors.warningColor, length: Toast.LENGTH_LONG);
        break;
      case 403:
        DialogueBoxes.dismissLoadingDialog();
        DialogueBoxes.flutterToastDialogueBox("${decodeResponse["message"]}",
            color: AppColors.warningColor, length: Toast.LENGTH_LONG);
        break;
      case 405:
        DialogueBoxes.dismissLoadingDialog();
        DialogueBoxes.flutterToastDialogueBox("${decodeResponse["message"]}",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
      case 500:
        DialogueBoxes.dismissLoadingDialog();
        DialogueBoxes.flutterToastDialogueBox("Server error",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
      case 501:
        DialogueBoxes.dismissLoadingDialog();
        DialogueBoxes.flutterToastDialogueBox("${decodeResponse["message"]}",
            color: AppColors.errorColor, length: Toast.LENGTH_LONG);
        break;
    }
  }
}
