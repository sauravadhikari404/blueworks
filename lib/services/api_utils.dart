import 'dart:async';
import 'dart:io';

import 'package:blue_works/constants/color.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../constants/keys.dart';
import '../constants/texts.dart';
import '../route/route_constants.dart';

class ApiUtils {
  ApiUtils._();
  factory ApiUtils() => instance;
  static ApiUtils instance = ApiUtils._();

  ///This is getResponse which is used to get data from server
  static Future<http.Response?> getResponse(
      {required String url, Map<String, String>? headers}) async {
    try {
      return await http
          .get(Uri.parse(url), headers: headers)
          .timeout(const Duration(seconds: 30));
    } on TimeoutException catch (_) {
      rethrow;
    } on SocketException catch (_) {
      rethrow;
    } catch (e) {
      rethrow;
    }
  }

  ///This is postResponse which is used to post data to sever
  static Future<http.Response?> postResponse(
      {required String url,
      Map<String, String>? headers,
      Map<String, dynamic>? body}) async {
    try {
      return await http
          .post(Uri.parse(url), headers: headers, body: body)
          .timeout(const Duration(seconds: 30));
    } on TimeoutException catch (_) {
      DialogueBoxes.dismissLoadingDialog();
      DialogueBoxes.flutterToastDialogueBox("Error occured in server !",
          color: AppColors.errorColor);
      rethrow;
    } on SocketException catch (_) {
      DialogueBoxes.dismissLoadingDialog();
      DialogueBoxes.flutterToastDialogueBox("Error occured in server !",
          color: AppColors.errorColor);
      rethrow;
    } catch (e) {
      DialogueBoxes.flutterToastDialogueBox("Unhandled error occurred !",
          color: AppColors.errorColor);
      rethrow;
    }
  }

  postMethodNoHeader(String url, body, {dynamic retry}) async {
    logger.d("|DEBUG: URL |", url);
    try {
      return await http.post(Uri.parse(url), body: body);
    } on SocketException catch (s) {
      logger.i("Socket", s.message);
      if (s.message == "No route to host") {
        DialogueBoxes.flutterToastDialogueBox(
            "Cannot Connect to Host. Please try Again.",
            color: AppColors.errorColor);
        Get.toNamed(
          MeroRoutesName.noInternetPage,
        );
      } else if (s.message == "Connection timed out") {
        DialogueBoxes.flutterToastDialogueBox("Connection Time out",
            color: AppColors.errorColor);
      } else {
        retry == null
            ? DialogueBoxes.flutterToastDialogueBox(TextConst.noInternetTxt,
                color: AppColors.errorColor)
            : Get.toNamed(
                MeroRoutesName.noInternetPage,
              );
      }
    } catch (e) {
      logger.i("Error", e);
    }
  }
}
