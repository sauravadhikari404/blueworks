import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/enum.dart';
import 'package:blue_works/local%20storage/local_storage.dart';
import 'package:blue_works/widgets/dialogue%20boxes/dialogue_boxes.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import '../models/gallery_show_case_model.dart';
import '../models/slide_show_model.dart';
import '../models/professional_category_model.dart';
import '../models/user_profile_model.dart';
import '../screens/auth screens/model/login_model.dart';
import '../screens/profession listing screen/model/professionals_model.dart';
import '../screens/top professionals screen/model/top_professionals_model.dart';

class ModelHandler {
  ModelHandler._();
  factory ModelHandler() => instance;
  static ModelHandler instance = ModelHandler._();

  static addDataToModel(
      {MH? modelManager, http.Response? response, var decodedResponse}) {
    switch (modelManager ?? "") {
      case MH.loginModel:
        LocalStorage()
            .setTokens(decodedResponse["access"], decodedResponse["refresh"]);
        DialogueBoxes.flutterToastDialogueBox("${decodedResponse["message"]}",
            color: AppColors.successColor);
        authCtrl.emailOrPhoneCtrl.text = "";
        authCtrl.passwordCtrl.text = "";
        Get.offAllNamed(MeroRoutesName.dashBoard);
        return loginModelFromJson(response!.body);
      case MH.register:
        authCtrl.firstNameCtrl.text = "";
        authCtrl.middleNameCtrl.text = "";
        authCtrl.lastNameCtrl.text = "";
        authCtrl.phoneCtrl.text = "";
        authCtrl.password1Ctrl.text = "";
        authCtrl.password2Ctrl.text = "";
        DialogueBoxes.flutterToastDialogueBox("${decodedResponse["message"]}",
            color: AppColors.successColor);
        Get.offAllNamed(MeroRoutesName.loginPage);
        break;
      case MH.professionalCategoryModel:
        return professionalCategoryModelFromJson(response!.body);
      case MH.slideShowModel:
        return slideShowModelFromJson(response!.body);
      case MH.userProfileModel:
        return userProfileModelFromJson(response!.body);
      case MH.topProfessionalsModel:
        return topProfessionalsModelFromJson(response!.body);
      case MH.professionalsModel:
        return professionalsModelFromJson(response!.body);
      case MH.galleryShowCaseModel:
        return galleryShowCaseModelFromJson(response!.body);
      case MH.changePassword:
        settingCtrl.oldPasswordCtrl.text = "";
        settingCtrl.newPasswordCtrl.text = "";
        settingCtrl.confirmPasswordCtrl.text = "";
        DialogueBoxes.flutterToastDialogueBox("Password changed successfully",
            color: AppColors.successColor, length: Toast.LENGTH_LONG);
        Get.back();
        break;
      case MH.logoutUser:
        LocalStorage().clearCredential();
        Get.offAllNamed(MeroRoutesName.loginPage);
    }
  }
}
