import 'package:blue_works/api%20routes/api_routes.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/constants/keys.dart';
import 'package:blue_works/enum.dart';
import 'package:blue_works/local%20storage/local_storage.dart';
import 'package:blue_works/services/api_utils.dart';
import 'package:blue_works/services/response_handler.dart';
import 'package:http/http.dart' as http;
import '../models/gallery_show_case_model.dart';
import '../models/slide_show_model.dart';
import '../models/professional_category_model.dart';
import '../screens/profession listing screen/model/professionals_model.dart';
import '../screens/top professionals screen/model/top_professionals_model.dart';
import '../models/user_profile_model.dart';
import '../screens/auth screens/model/login_model.dart';

class Services {
  Services._();
  factory Services() => instance;
  static Services instance = Services._();

  static Future<LoginModel> loginUser(
      {required String emailOrPhone, required String password}) async {
    try {
      String url = ApiRoutes.loginUser();
      Map<String, dynamic> body = {"phone": emailOrPhone, "password": password};
      Map<String, String> header = {"secret": Keys.secretKey};
      http.Response? response =
          await ApiUtils.postResponse(url: url, body: body, headers: header);
      logger.d("This is body and header and response",
          "$body $header ${response?.statusCode}");
      return ResponseHandler.postResponse(response!,
          modelManager: MH.loginModel);
    } catch (e) {
      rethrow;
    }
  }

  static Future<void> userRegister(
      {required String phone,
      required String password,
      required firstName,
      String? middleName,
      String? lastName,
      String? role}) async {
    try {
      String url = ApiRoutes.registerUser();
      Map<String, dynamic> body = {
        "country_code": "977",
        "phone": phone,
        "password": password,
        "first_name": firstName,
        "middle_name": middleName,
        "last_name": lastName,
        "role": role
      };
      http.Response? response =
          await ApiUtils.postResponse(url: url, body: body);
      return ResponseHandler.postResponse(response!, modelManager: MH.register);
    } catch (e) {
      rethrow;
    }
  }

  static Future<void> logoutUser() async {
    try {
      String token = await LocalStorage().getAccessToken();
      String url = ApiRoutes.logoutUser();
      http.Response? response =
          await ApiUtils.postResponse(url: url, headers: {"auth_token": token});
      return ResponseHandler.postResponse(response!,
          modelManager: MH.logoutUser);
    } catch (e) {
      rethrow;
    }
  }

  static Future<UserProfileModel?> fetchUserData() async {
    try {
      final token = await LocalStorage().getAccessToken();
      String url = ApiRoutes.userProfile();
      if (networkCtrl.connectionType.value == 1 ||
          networkCtrl.connectionType.value == 2) {
        http.Response? response = await ApiUtils.getResponse(
            url: url,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Bearer $token"
            });
        return ResponseHandler.getResponse(
            response: response!, modelManager: MH.userProfileModel);
      } else {
        return UserProfileModel();
      }
    } catch (e) {
      rethrow;
    }
  }

  static Future<List<ProfessionalCategoryModel>?>
      fetchProfessionalCategories() async {
    try {
      String token = await LocalStorage().getAccessToken();
      String url = ApiRoutes.fetchProfessionalCategories();
      http.Response? response = await ApiUtils.getResponse(url: url, headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token",
        "secret": Keys.secretKey
      });
      return ResponseHandler.getResponse(
          response: response!, modelManager: MH.professionalCategoryModel);
    } catch (e) {
      rethrow;
    }
  }

  static Future<List<SlideShowModel>?> fetchSlideShows() async {
    try {
      String url = ApiRoutes.fetchSlideShowImages();
      String token = await LocalStorage().getAccessToken();
      http.Response? response = await ApiUtils.getResponse(url: url, headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      });
      return ResponseHandler.getResponse(
          response: response!, modelManager: MH.slideShowModel);
    } catch (e) {
      rethrow;
    }
  }

  static Future<TopProfessionalsModel?> fetchTopProfessionals(
      {String searchText = "",
      double? latitude,
      double? longitude,
      required int page}) async {
    try {
      String url =
          ApiRoutes.fetchTopProfessionals(searchText: searchText, page: page);
      String token = await LocalStorage().getAccessToken();
      Map<String, String> headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token",
        "lat": latitude.toString(),
        "long": longitude.toString(),
        "distance": "50"
      };
      logger.i("This is url", url);
      logger.i("fetch top professionals header", headers);
      http.Response? response =
          await ApiUtils.getResponse(url: url, headers: headers);
      return ResponseHandler.getResponse(
          response: response!, modelManager: MH.topProfessionalsModel);
    } catch (e) {
      rethrow;
    }
  }

  static Future<ProfessionalsModel> fetchProfessionals(
      {String searchText = "",
      double? latitude,
      double? longitude,
      required int pk,
      required int page}) async {
    try {
      String token = await LocalStorage().getAccessToken();
      String url = ApiRoutes.fetchProfessionals(
          searchText: searchText, pk: pk, page: page);
      logger.i("This is professionals", url);
      Map<String, String> headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token",
        "lat": latitude.toString(),
        "long": longitude.toString(),
        "distance": "50"
      };
      logger.i("This is url", url);
      logger.i("This is headers", headers);
      http.Response? response =
          await ApiUtils.getResponse(url: url, headers: headers);
      return ResponseHandler.getResponse(
          response: response!, modelManager: MH.professionalsModel);
    } catch (e) {
      rethrow;
    }
  }

  static Future<List<GalleryShowCaseModel>> fetchGalleryShowCase(
      {int? id}) async {
    try {
      String token = await LocalStorage().getAccessToken();
      String url = ApiRoutes.fetchGalleryShowCase(id: id);
      http.Response? response = await ApiUtils.getResponse(url: url, headers: {
        "Content-Type": "application/json",
        "Authorization": "Bearer $token"
      });
      return ResponseHandler.getResponse(
          response: response!, modelManager: MH.galleryShowCaseModel);
    } catch (e) {
      rethrow;
    }
  }

  static Future<void> changePassword(
      {required String oldPassword, required String newPassword}) async {
    try {
      String token = await LocalStorage().getAccessToken();
      String url = ApiRoutes.changePassword();
      Map<String, String> headers = {"Authorization": "Bearer $token"};
      Map<String, dynamic> body = {
        "old_password": oldPassword,
        "new_password": newPassword
      };
      logger.i("This is body", body);
      http.Response? response =
          await ApiUtils.postResponse(url: url, headers: headers, body: body);
      logger.i("This is reponse", response?.statusCode);
      return ResponseHandler.postResponse(response!,
          modelManager: MH.changePassword);
    } catch (e) {
      rethrow;
    }
  }
}
