import 'package:blue_works/constants/local_storage_consts.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class LocalStorage {
  LocalStorage._();
  factory LocalStorage() => instance;
  static LocalStorage instance = LocalStorage._();

  late FlutterSecureStorage _secureStorage;
  bool _initialized = false;
  AndroidOptions androidOptions =
      const AndroidOptions(encryptedSharedPreferences: true);

  init() async {
    if (!_initialized) {
      _secureStorage = FlutterSecureStorage(aOptions: androidOptions);
      _initialized = true;
    }
  }

  ///Set refresh and access token
  setTokens(String accessToken, String refreshToken) {
    _secureStorage.write(
        key: LocalStorageConst.accessToken, value: accessToken);
    _secureStorage.write(
        key: LocalStorageConst.refreshToken, value: refreshToken);
  }

  ///get access token
  getAccessToken() {
    return _secureStorage.read(key: LocalStorageConst.accessToken);
  }

  ///get refresh token
  getRefreshToken() {
    return _secureStorage.read(key: LocalStorageConst.refreshToken);
  }

  ///clear token
  clearCredential() async {
    await _secureStorage.delete(key: LocalStorageConst.accessToken);
    await _secureStorage.delete(key: LocalStorageConst.refreshToken);
  }
}
