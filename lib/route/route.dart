import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/screens/auth%20screens/pages/login_page.dart';
import 'package:blue_works/screens/auth%20screens/pages/register_page.dart';
import 'package:blue_works/screens/bottom%20navigation%20bar/dashboard.dart';
import 'package:blue_works/screens/profession%20listing%20screen/page/profession_list_page.dart';
import 'package:blue_works/screens/profile/page/edit_profile_page.dart';
import 'package:blue_works/screens/setting%20screen/pages/change_password.dart';
import 'package:blue_works/screens/top%20professionals%20screen/page/top_professionals_page.dart';
import 'package:blue_works/widgets/no%20internet%20and%20server%20error%20widget/no_internet_widget.dart';
import 'package:get/get.dart';

import '../widgets/photo viewer/single_photo_viewer.dart';
import '../widgets/professionals widgets/professionals_detail_showing_widget.dart';

class MeroRoutes {
  static final routes = [
    GetPage(
        name: MeroRoutesName.loginPage,
        page: () => const LoginPage(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.registerPage,
        page: () => const RegisterPage(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.dashBoard,
        page: () => const DashBoard(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.editProfilePage,
        page: () => const EditProfilePage(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.professionListingPage,
        page: () => const ProfessionListingPage(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.changePasswordPage,
        page: () => const ChangePasswordPage(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.topProfessionalsPage,
        page: () => const TopProfessionalsPage(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.professionalsDetailPage,
        page: () => const ProfessionalDetailShowingWidget(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.singlePhotoViewerPage,
        page: () => const SinglePhotoViewerWidget(),
        transition: Transition.fadeIn),
    GetPage(
        name: MeroRoutesName.noInternetPage,
        page: () => const NoInternetWidget(),
        transition: Transition.fadeIn)
  ];
}
