class MeroRoutesName {
  MeroRoutesName._();
  factory MeroRoutesName() => instance;
  static MeroRoutesName instance = MeroRoutesName._();

  static const splashScreen = "/splash_screen";
  static const loginPage = "/login_page";
  static const registerPage = "/register_page";
  static const bottomNavigationPage = "/bottom_navigation_page";
  static const dashBoard = "/dashboard_page";

  ///Home page navigations
  static const professionListingPage = "/profession_listing_page";
  static const topProfessionalsPage = "/top_professionals_page";

  ///Setting page navigations
  static const editProfilePage = "/edit_profile";
  static const changePasswordPage = "/change_password_page";

  ///professionals navigations
  static const professionalsDetailPage = "/professionals_detail_page";
  static const singlePhotoViewerPage = "/single_photo_viewer_page";

  static const noInternetPage = "/no_internet_page";
}
