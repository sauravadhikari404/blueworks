import 'package:blue_works/constants/api_urls.dart';

class ApiRoutes {
  ApiRoutes._();
  factory ApiRoutes() => instance;
  static final ApiRoutes instance = ApiRoutes._();

  static String loginUser() {
    return "${ApiUrls.baseUrl}login/";
  }

  static String verifyToken() {
    return "${ApiUrls.baseUrl}token/verify/";
  }

  static String registerUser() {
    return "${ApiUrls.baseUrl}register/";
  }

  static String logoutUser() {
    return "${ApiUrls.baseUrl}logout/";
  }

  static String userProfile() {
    return "${ApiUrls.baseUrl}profile/";
  }

  static String fetchProfessionalCategories() {
    return "${ApiUrls.baseUrl}profession-category/";
  }

  static String fetchSlideShowImages() {
    return "${ApiUrls.baseUrl}slide-show/";
  }

  static String fetchTopProfessionals({String searchText = "", int page = 1}) {
    return "${ApiUrls.baseUrl}top-professionals/?page=$page&search=$searchText";
  }

  static String fetchProfessionals(
      {String searchText = "", required int pk, int page = 1}) {
    return "${ApiUrls.baseUrl}professionals/?page=$page&search=$searchText&pk=$pk";
  }

  static String fetchGalleryShowCase({int? id}) {
    return "${ApiUrls.baseUrl}gallery-showcase/?user=$id";
  }

  static String changePassword() {
    return "${ApiUrls.baseUrl}change-password/";
  }
}
