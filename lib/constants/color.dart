import 'package:flutter/material.dart';

class AppColors {
  AppColors._();
  factory AppColors() => instance;
  static final AppColors instance = AppColors._();

  static Color appThemeColor = const Color(0xff0067f5);
  static Color bodyColor = const Color(0xffF6F6F6);
  static const textColor = Colors.black;
  static const errorColor = Color(0xffFF001F);
  static const successColor = Color(0xff2B8376);
  static const warningColor = Color(0xffff9966);

  static const Color kPrimaryColor = Color(0xff4CB051);
  static const Color kPrimaryColorDark = Color(0xff14271F);
  static const Color kPrimaryColorDarkDim = Color(0xFF134B34);
  static const Color kPrimaryColorLight = Color.fromARGB(255, 102, 199, 107);
  static const Color kCommunityQuestionColor = Color(0xffEFB156);
  static const Color kLikedButtonBackgroundColor = Color(0xffFEEACC);
  static const Color kScafoldBacgroundColor = Color(0xffF0EDFE);
  static const Color kScafoldBacgroundColorDark = Color(0xFF14281F);
  static const Color kAppBarIconColorLT = Colors.white;
  static const Color kAppBarTextColorLT = Colors.white;
  static const Color kAppBarTextColorLTDark = Color(0xff292929);
  static const Color kPrimaryTextColor = Color(0xff292929);
  static const Color kPrimaryTextLightColor = Color(0xffffffff);
  static const Color kPrimaryTextColorLT = Color(0xff282828);
  static const Color kTextSubtitleColor = Color(0xff4D4D4D);
  static const Color kSamllInfoDetailColor = Color(0xff7A8892);
  static const Color kPrimaryButtonColor = Color(0xff4CAF51);
  static const Color kSecondaryButtonColor = Color(0xffFF9800);
  static const Color kFloatingActionIconColor = Color(0xFFFFFFFF);
  static const Color kShadowColorLT = Color(0x5AACACAC);
  static const Color kIconColorLT = Colors.white;
  static const Color kCardColorLT = Color(0xFFFEFEFF);
  static const Color kContainerColorLT = Color(0xFFFEFEFF);
  static const Color kCardColorDK = Color(0xFF1C543F);
}
