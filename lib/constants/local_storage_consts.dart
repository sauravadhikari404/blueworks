class LocalStorageConst {
  LocalStorageConst._();
  factory LocalStorageConst() => instance;
  static LocalStorageConst instance = LocalStorageConst._();

  static const String accessToken = "accessToken";
  static const String refreshToken = "refreshToken";
}
