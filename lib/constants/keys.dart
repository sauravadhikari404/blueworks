import 'package:logger/logger.dart';

class Keys {
  Keys._();
  factory Keys() => instance;
  static Keys instance = Keys._();

  static String secretKey =
      "django-insecure-ib7(k*v9p-d@\$4k)4h!g5e)iptgqz7_--9!frw-6z_%sgw2r71";

  static RegExp passwordRegx =
      RegExp(r'^(?=.*[A-Z])(?=.*\d{3})(?=.*[a-zA-Z]).{8,15}$');
}

Logger logger = Logger();
