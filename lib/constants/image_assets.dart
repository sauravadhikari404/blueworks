import 'package:flutter/material.dart';

class AssetConst {
  AssetConst._();
  factory AssetConst() => instance;
  static final AssetConst instance = AssetConst._();

  static String loginPng = "assets/images/login.png";
  static String createAccountPng = "assets/images/create_account.png";
  static const bLogo = "assets/logos/b_logo.png";
  static const placeHolder = "assets/images/blue_works_place_holder.jpeg";
  static const profile = "assets/icons/profile.png";
  static const address = "assets/icons/address.png";
  static const profession = "assets/icons/profession.png";
  static const about = "assets/icons/about.svg";
  static const gallery = "assets/icons/gallery.svg";

  static const loadingSpinner = "assets/loading/loading_spinner.gif";
  static const noInternet = "assets/icons/no_internet.json";

  ///Flutter icons constant which is used frequently used.........................................
  static IconData goBack = Icons.arrow_back_ios_new;
  static IconData phone = Icons.phone;
}
