import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/widgets.dart';

EdgeInsets bodyPadding(BuildContext context) {
  return EdgeInsets.symmetric(
      horizontal: 0.040.w(context), vertical: 0.020.h(context));
}

EdgeInsets bodyOnlyPadding(BuildContext context) {
  return EdgeInsets.only(
      left: 0.040.w(context), right: 0.040.w(context), top: 0.020.h(context));
}

EdgeInsets bodyLeftRightPadding(BuildContext context) {
  return EdgeInsets.only(left: 0.040.w(context), right: 0.040.w(context));
}

unFocus() {
  FocusManager.instance.primaryFocus?.unfocus();
}
