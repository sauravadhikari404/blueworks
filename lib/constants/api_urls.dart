class ApiUrls {
  ApiUrls._();
  factory ApiUrls() => instance;
  static final ApiUrls instance = ApiUrls._();

  static const String baseUrl = "http://141.148.215.182:8000/api/v2/";
  static const String mediaUrl = "http://141.148.215.182:8000/";
}
