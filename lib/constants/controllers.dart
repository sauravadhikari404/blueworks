import 'package:blue_works/controller/connectivity_check_controller.dart';
import 'package:blue_works/controller/global_controller.dart';
import 'package:blue_works/controller/professional_category_controller.dart';
import 'package:blue_works/controller/theme_controller.dart';
import 'package:blue_works/screens/auth%20screens/controller/auth_controller.dart';
import 'package:blue_works/screens/home%20screen/controller/home_page_controller.dart';
import 'package:blue_works/screens/profession%20listing%20screen/controller/professionals_controller.dart';
import 'package:blue_works/screens/profile/controller/edit_profile_controller.dart';
import 'package:blue_works/screens/setting%20screen/controller/setting_controller.dart';

import '../controller/professionals_detail_page_controller.dart';

GlobalController globalCtrl = GlobalController.instance;
AuthController authCtrl = AuthController.instance;
ThemeController themeCtrl = ThemeController.instance;
ProfessionalCategoryController profCatCtrl =
    ProfessionalCategoryController.instance;
EditProfileController editProfCtrl = EditProfileController.instance;
SettingController settingCtrl = SettingController.instance;
HomePageController homePageCtrl = HomePageController.instance;
GetXNetworkManager networkCtrl = GetXNetworkManager.instance;
ProfessionalsController profCtrl = ProfessionalsController.instance;
ProfessionalDetailPageController profDetailPageCtrl =
    ProfessionalDetailPageController.instance;
