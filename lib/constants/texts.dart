class TextConst {
  TextConst._();
  factory TextConst() => instance;
  static final TextConst instance = TextConst._();

  ///Dashboard Texts
  static const String home = "Home";
  static const String setting = "Setting";

  ///Login page Texts
  static const String phoneOrEmail = "Phone/Email";
  static const String password = "Password";
  static const String login = "Login";
  static const String noAccount = "Don't have an accout";
  static const String signUp = "Sign Up";

  ///Register Page Texts
  static const String fName = "First Name";
  static const String mName = "Middle Name";
  static const String lName = "Last Name";
  static const String phone = "Phone";
  static const String confirmPass = "Confirm Password";
  static const String alreadyAccount = "Already have an account? ";

  ///Home Page Texts
  static const String welcome = "Welcome";
  static const String topProf = "Top Professionals";
  static const String hi = "Hi";
  static const String viewAll = "View All";

  ///Setting Page Texts
  static const String general = "GENERAL";
  static const String editProf = "Edit Profile";
  static const String changeTheme = "Change Theme";
  static const String logout = "Logout";
  static const String security = "SECURITY";
  static const String changePass = "Change Password";

  ///Edit Profile Page Texts
  static const String changePP = "Change profile picture";
  static const String dob = "Dob";
  static const String edit = "Edit";
  static const String profile = "Profile";

  ///Change Password Page Texts
  static const String oldPass = "Old Password";
  static const String newPass = "New Password";

  ///Professionals Detail Page Texts
  static const String profDetailPage = "Professional Detail Page";

  ///No Internet
  static const String noInternetTxt = "Please check your internet connection";
}
