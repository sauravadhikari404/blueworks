import 'dart:async';
import 'dart:convert';

import 'package:blue_works/api%20routes/api_routes.dart';
import 'package:blue_works/constants/color.dart';
import 'package:blue_works/constants/controllers.dart';
import 'package:blue_works/local%20storage/local_storage.dart';
import 'package:blue_works/route/route_constants.dart';
import 'package:blue_works/services/api_utils.dart';
import 'package:blue_works/widgets/extension/extension.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

import 'constants/keys.dart';
import 'widgets/dialogue boxes/dialogue_boxes.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AnimationController? _controller;

  @override
  void initState() {
    super.initState();
    globalCtrl.setStatusBarColor(colorss: AppColors.appThemeColor);
    _verifyToken();
  }

  static Future<void> _verifyToken() async {
    String token = await LocalStorage().getAccessToken() ?? "empty token";
    String url = ApiRoutes.verifyToken();
    try {
      http.Response? response =
          await ApiUtils.postResponse(url: url, body: {"token": token});
      if (response?.statusCode == 200) {
        Get.offAllNamed(MeroRoutesName.dashBoard);
      } else if (response?.statusCode == 401) {
        var data = jsonDecode(response!.body);
        Get.offAllNamed(MeroRoutesName.loginPage);
        DialogueBoxes.flutterToastDialogueBox("${data["detail"]}",
            color: AppColors.errorColor);
      } else {
        var data = jsonDecode(response!.body);
        DialogueBoxes.flutterToastDialogueBox("${data["detail"]}",
            color: AppColors.errorColor);
      }
    } catch (e) {
      if (e.toString() == "Connection failed") {
        Get.offAllNamed(MeroRoutesName.noInternetPage);
      } else {
        logger.i("Exception occurred", e.toString());
      }
    }
  }

  @override
  void dispose() {
    _controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.bodyColor,
      appBar: AppBar(
        backgroundColor: AppColors.bodyColor,
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle(
            statusBarColor: AppColors.bodyColor,
            statusBarIconBrightness: Brightness.dark),
      ),
      body: Center(
        child: Text(
          "B",
          style: TextStyle(
              fontSize: 0.2.toResponsive(context),
              color: AppColors.appThemeColor),
        ),
      ),
    );
  }
}
